//
//  ChartView.h
//  Metrics
//
//  Created by Desi Nikolova on 4/1/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface ChartView : UIView

@property(nonatomic, assign) BOOL periodDaily;//corresponds to the days in the month, else 12 months /last year/.
@property(nonatomic, retain) NSMutableArray *chartData;
@property(nonatomic, retain) IBOutlet UILabel *noDataLabel, *visitsPeriodData;

-(int)yAxisIntervals;
-(int)xAxisIntervals;

@end
