//
//  PIeChartView.m
//  Metrics
//
//  Created by Desi Nikolova on 4/27/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "PieChartView.h"
#import "CountryData.h"
#import "AppSettings.h"

@implementation PieChartView

@synthesize assignedProperty, dataByCountry;

#define kChartPadding 8.0f
#define kMaxNumberOfSectors 7 // With indicated country
#define kSectorIndicatorsOffset 20.0

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [AppSettings instance];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //Calculate the angles based on the latest data for this property:
    //[[AppSettings instance] loadColors];
   // [self computeAngles];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    //CGContextClearRect(context, rect);
    CGContextSaveGState(context);
    
    CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);//white
    CGContextSetLineWidth(context, 2.0);
    //
    
    //Inverse the matrix since the labels render upside-down:
    //CGAffineTransform trans = CGAffineTransformMakeScale(1, -1);
    //CGContextSetTextMatrix(context, trans);
    
    float circleLen = M_PI * 2;
    CGPoint center = CGPointMake(rect.size.width / 2, rect.size.height / 2);
    float r = 80;
    float t = 0.0, t_prev = 0.0;
    
    
    for(int i = 0; i < angles.count; i++)
    {
        NSNumber *angle = [angles objectAtIndex:i];
        t = angle.floatValue * circleLen + t_prev;
        CGMutablePathRef fillPath = CGPathCreateMutable();
        
        CGPathMoveToPoint( fillPath, NULL, center.x, center.y);
        CGPathAddArc(fillPath, NULL, center.x, center.y, r, t_prev, t, false);
        CGPathCloseSubpath(fillPath);
        
        CGContextBeginPath(context);
        CGContextAddPath(context, fillPath);
        //
        CGContextSaveGState(context);
        NSLog(@"object at index %i", i);
        UIColor *sectorColor = [[AppSettings instance].pieChartColors objectAtIndex:i];
        CGContextSetFillColorWithColor(context, sectorColor.CGColor);
        CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);
        CGContextFillPath(context);
        //CGContextStrokePath(context);
        CGContextRestoreGState(context);
        
        CGPathRelease(fillPath);
        
        t_prev = t;
        
    }
    
    t = 0.0, t_prev = 0.0;
    
    
    CGContextSetRGBStrokeColor(context, 77.0/255.0, 222.0/255.0, 77.0/255.0, .88);//green.
    CGContextSetLineWidth(context, 1.0);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    for(int i = 0; i < angles.count; i++)
    {
        NSNumber *angle = [angles objectAtIndex:i];
        t = angle.floatValue * circleLen + t_prev;
        CGMutablePathRef fillPath = CGPathCreateMutable();
        
        CGPathMoveToPoint( fillPath, NULL, center.x, center.y);
        CGPathAddArc(fillPath, NULL, center.x, center.y, r, t_prev, t, false);
        CGPathCloseSubpath(fillPath);
        
        CGContextBeginPath(context);
        CGContextAddPath(context, fillPath);
        //
        CGContextSaveGState(context);
        //CGContextSetFillColorWithColor(context, [UIColor lightGrayColor].CGColor);
        //CGContextFillPath(context);
        CGContextStrokePath(context);
        CGContextRestoreGState(context);
        
        CGPathRelease(fillPath);
        
        t_prev = t;
        
        //THere is a lag in the rendering - so wait for the last iteration:
        if(i == angles.count - 1)
        {
            //Add a small circle in the center:
            CGContextSaveGState(context);
            CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, .88);
            CGContextSetRGBStrokeColor(context, 77.0/255.0, 222.0/255.0, 77.0/255.0, .88);
            float rad = 6.0;
            CGRect target = CGRectMake(center.x - rad, center.y -  rad, rad * 2, rad * 2);
            CGContextAddEllipseInRect(context, target);
            CGContextStrokeEllipseInRect(context, target);
            CGContextFillEllipseInRect(context, target);
            CGContextRestoreGState(context);
            
            //Add indicators:
            [self addSectorIndicators:r center:center context:context];
            
        }
        
    }
    
    
    
}

-(void)addSectorIndicators:(float)mainRadius center:(CGPoint)center context:(CGContextRef)context
{
    // A line from a mid point of a each sector & a lable with the name of the sector:
    
    float radius = mainRadius - kSectorIndicatorsOffset;
    
    // Will draw in the middle of each sector:
    
    float circleLen = M_PI * 2;
    
    float t = 0.0, t_prev = 0.0;
    
    float real_t = 0.0;
    
    CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);
    
    //Lable drawing:
    
    //Configure the label formatting for each label:
    
    CGContextSetCharacterSpacing(context, 1.0);
    CGContextSetTextDrawingMode(context, kCGTextFill);
    CFStringRef string = NULL;
    CFStringRef fontName = CFSTR("Helvetica");;
    CTFontRef font = CTFontCreateWithName(fontName, 14.0, NULL);
    // Initialize the string, font, and context
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat components[] = { 1.0, 1.0, 1.0, 1.0 };
    
    CGColorRef white = CGColorCreate(rgbColorSpace, components);
    
    CGColorSpaceRelease(rgbColorSpace);
    CFStringRef keys[] = { kCTFontAttributeName, kCTForegroundColorAttributeName };
    CFTypeRef values[] = { font, white };
    
    CFDictionaryRef attributes =
    CFDictionaryCreate(kCFAllocatorDefault, (const void**)&keys,
                       (const void**)&values, sizeof(keys) / sizeof(keys[0]),
                       &kCFTypeDictionaryKeyCallBacks,
                       &kCFTypeDictionaryValueCallBacks);
    
    CGAffineTransform trans = CGAffineTransformMakeScale(1, -1);
    CGContextSetTextMatrix(context, trans);

    CountryData *country;
    
    for(int i = 0; i < angles.count; i++)
    {
        if(dataByCountry)
            country = [assignedProperty.countriesData objectAtIndex:i];
        else{
            //...
        }
        NSNumber *angle = [angles objectAtIndex:i];
        
        if(angle.floatValue < 0.04)
            continue;
        
        t = (angle.floatValue * circleLen) * .5 + t_prev;
        real_t =  (angle.floatValue * circleLen) + t_prev;
        
        CGPoint startingPoint = PointOnCircle(radius, t, center);
        
        float end_pt = startingPoint.x + 80.0;
        
        NSString *labelText = [NSString stringWithFormat:@"%@(%i)", country.countryName, country.visits];
        if(dataByCountry)
            string = (__bridge CFStringRef)labelText;
        else
            if(i == 0){
                labelText = [NSString stringWithFormat:@"Browsers(%i)", assignedProperty.visitsFromBrowsers];
                string = (__bridge CFStringRef)labelText;
            }else if (i == 1){
                labelText = [NSString stringWithFormat:@"Mobile devices(%i)", assignedProperty.visitsFromMobileDevices];
                string = (__bridge CFStringRef)labelText;//can
            }
        //string = CFStringCreate(NULL, NULL, countryName);
        CFAttributedStringRef attrString =
        
        CFAttributedStringCreate(kCFAllocatorDefault, string, attributes);
        //
        CGSize labelSize = [self boundingWidthForHei:20.0 forString:attrString];
        float label_x_pos = end_pt - labelSize.width;
        
        if(real_t > M_PI / 2 && real_t < M_PI * 1.75)
        {
            end_pt = startingPoint.x - 80.0;
            label_x_pos = startingPoint.x - labelSize.width;
        }
        
        if(label_x_pos < 5)
            label_x_pos = 5.0;
        
       // NSLog(@"Angle num %i - radius %f, t %f, center(%f, %f); angle %f ",i, radius, t, center.x, center.y, angle.floatValue);
        /*
        CGContextSaveGState(context);
        CGContextMoveToPoint(context, startingPoint.x, startingPoint.y);
        CGContextAddLineToPoint(context, end_pt, startingPoint.y);
        CGContextDrawPath(context, kCGPathStroke);
        CGContextRestoreGState(context);
        */
        //Add a lable:
        //using the x/y form above for secors with angle greater tan a tresholld value;
        
        //compute the size
        CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1.0);//or dark green..
        //CGContextFillRect(context, CGRectMake(label_x_pos - 2.0, startingPoint.y -  21.0, 80, 18));
        CTLineRef line = CTLineCreateWithAttributedString(attrString);
        // Set text position and draw the line into the graphics context
        CGContextSetTextPosition(context, label_x_pos, startingPoint.y - 3.0);
        CTLineDraw(line, context);
        CFRelease(line);
        
        t_prev = real_t;
        
    }
    
}

//Accept the radius, angle, and center:

CGPoint PointOnCircle(float radius, float angle, CGPoint origin)
{
    //
    double x = (float)(radius * cos(angle) + origin.x);
    double y = (float)(radius * sin(angle) + origin.y);
    
    return CGPointMake(x, y);
}


- (CGSize)boundingWidthForHei:(CGFloat)hei forString:(CFAttributedStringRef)stringRef
{
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(stringRef);
    CGSize suggestedSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0), NULL, CGSizeMake(10000, hei), NULL);
    CFRelease(framesetter);
    return suggestedSize;
}

//Compute the angles in radians before rendering the chart:

-(void)computeAngles
{
    angles = [[NSMutableArray alloc] init];
    
    
    //testing the color pallete:
    /*
    [angles addObject:[NSNumber numberWithFloat:.10]];
    [angles addObject:[NSNumber numberWithFloat:.10]];
    [angles addObject:[NSNumber numberWithFloat:.10]];
    [angles addObject:[NSNumber numberWithFloat:.10]];
    [angles addObject:[NSNumber numberWithFloat:.10]];
    [angles addObject:[NSNumber numberWithFloat:.10]];
    [angles addObject:[NSNumber numberWithFloat:.10]];
    [angles addObject:[NSNumber numberWithFloat:.10]];
    [angles addObject:[NSNumber numberWithFloat:.10]];
     return;
    */
    
    
    if(dataByCountry)
    {
    
        float totalVisitsFromCountries = 0;
    
        CountryData *country;
        for(int i = 0; i < assignedProperty.countriesData.count; i++)
        {
            country = [assignedProperty.countriesData objectAtIndex:i];
            totalVisitsFromCountries += country.visits;
            
        }
    
        //store as fractions of 1:
        
        float others = 0.0;
        for(int i = 0; i < assignedProperty.countriesData.count; i++)
        {
            country = [assignedProperty.countriesData objectAtIndex:i];
            if(country.visits > 2)
            {
                [angles addObject:[NSNumber numberWithDouble:(double)country.visits / totalVisitsFromCountries]];
                
                NSLog(@"new angle %f", (float)country.visits / totalVisitsFromCountries);
            }else{
                others += (float)country.visits;
            }
        }
        
        if(others > 2 || angles.count >= kMaxNumberOfSectors)
        {
            [angles addObject:[NSNumber numberWithDouble:others / totalVisitsFromCountries]];
            otherCountriesSector = YES;
            NSLog(@"others -  angle %f", others / totalVisitsFromCountries);
        }else{
            otherCountriesSector = NO;
        }
    }else{
        //Only 2 sectors for now - allow the selection of more:
        //
        float totalVisits = assignedProperty.visitsFromBrowsers + assignedProperty.visitsFromMobileDevices;
        
        [angles addObject:[NSNumber numberWithFloat:(float)assignedProperty.visitsFromBrowsers / totalVisits]];
        [angles addObject:[NSNumber numberWithFloat:(float)assignedProperty.visitsFromMobileDevices / totalVisits]];
    }
    
}

-(void)loadColors
{
    //!! TO DO - take the colors from the UI schema of the app (storyboard)
    
    
     //153.0/255.0 156.0/255.0 156.0/255.0
    UIColor *color1 = [UIColor colorWithRed:77.0/255.0 green:222.0/77.0 blue:89.0/255.0 alpha:.55];
     //156.0/255.0       51.0/255.0       100.0/255.0
    UIColor *color2 = [UIColor colorWithRed:156.0/255.0 green:51.0/255.0 blue:100.0/255.0 alpha:1.0];
     //255.0/255.0    253.0/255.0  209.0/255.0
    UIColor *color3 = [UIColor colorWithRed:255.0/255.0 green:253.0/255.0 blue:209.0/255.0 alpha:1.0];
     //201.0/255.0    255.0/255.0  255.0/255.0
    UIColor *color4 = [UIColor colorWithRed:201.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];
     //102.0/255.0    4.0/255.0    95.0/255.0
    UIColor *color5 = [UIColor colorWithRed:102.0/255.0 green:4.0/255.0 blue:95.0/255.0 alpha:1.0];
     //255.0/255.0    128.0/255.0  130.0/255.0
    UIColor *color6 = [UIColor colorWithRed:255.0/255.0 green:128.0/255.0 blue:130.0/255.0 alpha:1.0];
     //0.0/255.0      105.0/255.0  196.0/255.0
    UIColor *color7 = [UIColor colorWithRed:0.0/255.0 green:105.0/255.0 blue:196.0/255.0 alpha:1.0];
     //205.0/255.0    206.0/255.0  250.0/255.0
    UIColor *color8 = [UIColor colorWithRed:205.0/255.0 green:206.0/255.0 blue:250.0/255.0 alpha:1.0];
     //2.0/255.0      10.0/255.0   120.0/255.0
    UIColor *color9 = [UIColor colorWithRed:2.0/255.0 green:10.0/255.0 blue:120.0/255.0 alpha:1.0];
     //255.0/255.0    25.0/255.0   244.0/255.0
    UIColor *color10 = [UIColor colorWithRed:255.0/255.0 green:25.0/255.0 blue:244.0/255.0 alpha:1.0];
     //255.0/255.0    252.0/255.0  87.0/255.0
    UIColor *color11 = [UIColor colorWithRed:255.0/255.0 green:252.0/255.0 blue:87.0/255.0 alpha:1.0];
     //0.0/255.0      255.0/255.0  251.0/255.0
    UIColor *color12 = [UIColor colorWithRed:0.0/255.0 green:255.0/255.0 blue:251.0/255.0 alpha:1.0];
    
    sectorColors = [NSMutableArray arrayWithObjects:color1,color2,color3,color4,color5,color6,color7,color8,color9,color10,color11,color12, nil];
    
}


@end
