//
//  WebPropertyViewController.m
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/9/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "WebPropertyViewController.h"
#import "ProfileViewController.h"
#import "DataManager.h"
#import "WebProperties.h"
#import "PlotGallery.h"
#import "PlotItem.h"
#import "CorePlot-CocoaTouch.h"
#import "DataManager.h"

@interface WebPropertyViewController ()

@end

@implementation WebPropertyViewController


@synthesize scrollView, pageControl, viewControllers;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    _activityBox.layer.borderColor = [UIColor whiteColor].CGColor;
    _activityBox.layer.borderWidth = 1.0;
    _activityBox.layer.borderColor = [UIColor whiteColor].CGColor;
    _activityBox.layer.cornerRadius = 11;
    _activityBox.layer.masksToBounds = YES;
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadUIWithData) name:@"InitialAnalyticsDataAvailable" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:@"ReloadDone" object:nil];
}


-(void)loadUIWithData
{
    if(![DataManager instance].webPropertiesWithData.count)
        return;
    
    _activityView.hidden = YES;
    // view controllers are created lazily
    // in the meantime, load the array with placeholders which will be replaced on demand
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    //Here use the current user existing web properties
    for(int i = 0; i < [DataManager instance].webPropertiesWithData.count; i++)
    {
        WebProperties *properties = [[DataManager instance].webPropertiesWithData objectAtIndex:i];
        
        if(properties.muted)
          continue;
        
        ProfileViewController *profile = [sb instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        profile.assignedProperty = [[DataManager instance].webPropertiesWithData objectAtIndex:i];
        profile.parentView = self;
        [controllers addObject:profile];
    }
    
    WebProperties *profile = [[DataManager instance].webPropertiesWithData objectAtIndex:0];
    _currentProfleTitle.text = profile.assignedProfile.websiteUrl;
    
    self.viewControllers = controllers;
    //
    // a page is the width of the scroll view
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize =
    CGSizeMake(CGRectGetWidth(self.scrollView.frame) * controllers.count, CGRectGetHeight(self.scrollView.frame));
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    self.pageControl.numberOfPages = controllers.count;
    self.pageControl.currentPage = 0;
    
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    //
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    NSLog(@"loading profile controllers");
    
    for(ProfileViewController *profile in viewControllers)
        [profile updateUI];
}

// rotation support for iOS 5.x and earlier, note for iOS 6.0 and later this will not be called
//
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    // return YES for supported orientations
    return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
#endif

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    // remove all the subviews from our scrollview
    for (UIView *view in self.scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    NSUInteger numPages = viewControllers.count;
    
    // adjust the contentSize (larger or smaller) depending on the orientation
    self.scrollView.contentSize =
    CGSizeMake(CGRectGetWidth(self.scrollView.frame) * numPages, CGRectGetHeight(self.scrollView.frame));
    //
    [self loadScrollViewWithPage:self.pageControl.currentPage - 1];
    [self loadScrollViewWithPage:self.pageControl.currentPage];
    [self loadScrollViewWithPage:self.pageControl.currentPage + 1];
    [self gotoPage:NO]; // remain at the same page (don't animate)
}

- (void)loadScrollViewWithPage:(NSUInteger)page
{
    if (page >= viewControllers.count)
        return;
    
    // replace the placeholder if necessary
    ProfileViewController *controller = [self.viewControllers objectAtIndex:page];
    //
    //NSLog(@"current page # %i", self.pageControl.currentPage);
    
    if(page == self.pageControl.currentPage)
    {
        _currentProfile = controller;
         controller.assignedProperty = [[DataManager instance].webPropertiesWithData objectAtIndex:self.pageControl.currentPage];
    }
    
    if(self.pageControl.currentPage == page)
    {
        //Reload the data:
        [DataManager instance]._currentProperty = _currentProfile.assignedProperty;
        [[DataManager instance] load];
    }
    
     [controller updateUI];
    
    _currentProfleTitle.text = _currentProfile.assignedProperty.assignedProfile.websiteUrl;
    
    controller.view.frame = CGRectMake(0, 0, 320, self.view.frame.size.height - scrollView.frame.origin.y);
    ///
    CGRect frame = self.scrollView.frame;
    frame.origin.x = CGRectGetWidth(frame) * page;
    frame.origin.y = 0;
    controller.view.frame = frame;
    
    if (controller.view.superview == nil)
    {
        [self addChildViewController:controller];
        [self.scrollView addSubview:controller.view];
        [controller didMoveToParentViewController:self];
    }
}

// at the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = CGRectGetWidth(self.scrollView.frame);
    NSUInteger page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // a possible optimization would be to unload the views+controllers which are no longer visible
}

- (void)gotoPage:(BOOL)animated
{
    //NSLog(@"going to which page %d", self.pageControl.currentPage);
    NSInteger page = self.pageControl.currentPage;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
	// update the scroll view to the appropriate page
    CGRect bounds = self.scrollView.bounds;
    bounds.origin.x = CGRectGetWidth(bounds) * page;
    bounds.origin.y = 0;
    [self.scrollView scrollRectToVisible:bounds animated:animated];
}

- (IBAction)changePage:(id)sender
{
    [self gotoPage:YES];    // YES = animate
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)settingsBtnAction:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToSettings" object:nil];
}


-(IBAction)refreshData:(id)sender
{
    _activityView.hidden = NO;
    [[DataManager instance] refreshData];
}

-(void)busyView
{
    _activityView.hidden = NO;
}

-(void)reloadData
{
    for(ProfileViewController *profile in viewControllers)
    {
        profile.chartView.chartData = profile.assignedProperty.chartData;
        [profile updateUI];
    }
    _activityView.hidden = YES;
}


@end
