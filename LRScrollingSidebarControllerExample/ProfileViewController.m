//
//  ProfileViewController.m
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/15/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "ProfileViewController.h"
#import "PlotItem.h"
#import "AppSettings.h"
#import "DataManager.h"
#import "WebPropertyViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

@synthesize assignedProperty;
@synthesize plotRenderView, hostingView, plot;
@synthesize _theme;
@synthesize chartView;
@synthesize parentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    //Add the data
    [chartView setNeedsDisplay];//will force redisplay
    
    _detailsViewCotroller = [sb instantiateViewControllerWithIdentifier:@"MetricsDetailedViewController"];
   
    /**/
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeDetailedMetricsView) name:@"CloseDetailedMetricsView" object:nil];
    
}

-(void)closeDetailedMetricsView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)drawPlot
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateUI
{
    NSLog(@"updating the UI");
    //
    visitsToday.text = [NSString stringWithFormat:@"%i", (int)assignedProperty.visits ];
    visitorsToday.text = [NSString stringWithFormat:@"%i", (int)assignedProperty.visitors ];
    pageviewsToday.text = [NSString stringWithFormat:@"%i", (int)assignedProperty.pageviews ];
    //
    [_periodsSegmControl setTitle:[AppSettings instance].sinceMonth forSegmentAtIndex:2];
    [_periodsSegmControl setTitle:[AppSettings instance].sinceYear forSegmentAtIndex:3];
    _periodsSegmControl.selectedSegmentIndex = [AppSettings instance].analyticsChartPeriod;
    
    NSLog(@"segm control - new options - %@, %@",[AppSettings instance].sinceMonth ,[AppSettings instance].sinceYear );
    [_periodsSegmControl setNeedsDisplay];
    
    titleForPeriod.text = @"";
    switch ([AppSettings instance].analyticsChartPeriod)
    {
        case PERIOD_IS_TODAY:
        {
            titleForPeriod.text = @"Today";
        }break;
        case PERIOD_IS_THIS_MONTH:
        {
            titleForPeriod.text = @"This month";
        }break;
        case PERIOD_IS_SINCE_MONTH:
        {
            titleForPeriod.text = [AppSettings instance].sinceMonth;
        }break;
        case PERIOD_IS_SINCE_YEAR:
        {
            titleForPeriod.text = [AppSettings instance].sinceYear;
        }break;
        default:
            break;
    }
    
    //TO DO - darw the chart
    [chartView setNeedsDisplay];
}


-(IBAction)newPeriodSelectedBtnAction:(UISegmentedControl*)sender
{
    //to do - Reload the data
    
    [AppSettings instance].analyticsChartPeriod = sender.selectedSegmentIndex;
    
    [parentView busyView];
    
    [[DataManager instance] refreshData];
    
    titleForPeriod.text = @"";
    switch ([AppSettings instance].analyticsChartPeriod)
    {
        case PERIOD_IS_TODAY:
        {
            titleForPeriod.text = @"Today";
        }break;
        case PERIOD_IS_THIS_MONTH:
        {
            titleForPeriod.text = @"This month";
        }break;
        case PERIOD_IS_SINCE_MONTH:
        {
            titleForPeriod.text = [AppSettings instance].sinceMonth;
        }break;
        case PERIOD_IS_SINCE_YEAR:
        {
            titleForPeriod.text = [AppSettings instance].sinceYear;
        }break;
        default:
            break;
    }
}

-(IBAction)displayDetailedViewController:(UIButton*)sender
{
    
    [self presentViewController:_detailsViewCotroller animated:YES completion:^{
        
        _detailsViewCotroller.assignedProfile = assignedProperty;
        
        if(sender.tag == 22)
        {
            //agent metrics
            _detailsViewCotroller.dataByCountry = NO;
            [[DataManager instance] loadVisitsWithPlatformInfoForProfile:self.assignedProperty];
        }else if(sender.tag == 33)
        {
            //country (location) metrics
            _detailsViewCotroller.dataByCountry = YES;
            [[DataManager instance] loadVisitsByCountryForProfile:self.assignedProperty];
        }
    }];
    
    //Chart - may hide because it might take too much time.
}


@end
