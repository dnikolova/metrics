//
//  HowTosViewController.h
//  Metrics
//
//  Created by Desi Nikolova on 3/30/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
    CONTENT_MONITORING_YOUR_DATA,//additional
    CONTENT_ADDING_MORE_WEBSITES,//additional
    CONTENT_USING_GOOGLE_ANALYTICS,//additional
    //
    CONTENT_CONFIGURING_DATA_PERIOD,//how tos
    CONTENT_CLEARING_CURRENT_SESSION,//how tos
    CONTENT_REFRESHING_THE_DATA//how to
}contentType;

@interface HowTosViewController : UIViewController
{
    IBOutlet UIScrollView *_howToContentView;
    NSString *monitoringYourData;
    NSString *addingMoreWebSites;
    NSString *usingGoogleAnalyticsOnline;
    NSString *configuringDataPeriod;
    NSString *clearingCurrentSession;
    NSString *refreshingTheData;
    IBOutlet UITextView *contentView;
    //
    IBOutlet UIView *configureView, *clearSession, *refreshingData;
    IBOutlet UIView *monitoringDataView, *usingGoogleView, *addMoreWebsitesView;
}

@property(nonatomic, retain) IBOutlet UILabel *titleLable;
@property(nonatomic, retain) IBOutlet UIButton *closeBtn;
@property(nonatomic, assign) int contentToDisplay;

-(void)reload;

@end
