//
//  EducationViewController.m
//  MetricsApp
//
//  Created by Desi Nikolova on 3/27/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "EducationViewController.h"

@interface EducationViewController ()

@end

@implementation EducationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    _howTosViewController = [sb instantiateViewControllerWithIdentifier:@"HowTosViewController"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)monitoringData:(id)sender
{
    //to do - fill content
    [self presentViewController:_howTosViewController animated:YES completion:^{
        _howTosViewController.titleLable.text = @"Monitoring my data";
        _howTosViewController.contentToDisplay = CONTENT_MONITORING_YOUR_DATA;
        [_howTosViewController reload];
        [_howTosViewController.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    
}

-(IBAction)usingAnalyticsOnline:(id)sender
{
    //to do - fill content
    [self presentViewController:_howTosViewController animated:YES completion:^{
        _howTosViewController.titleLable.text = @"Using Google Analytics on-line";
        _howTosViewController.contentToDisplay = CONTENT_USING_GOOGLE_ANALYTICS;
        [_howTosViewController reload];
        [_howTosViewController.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

-(IBAction)addingMyWebSite:(id)sender
{
    //to do - fill content
    [self presentViewController:_howTosViewController animated:YES completion:^{
        _howTosViewController.titleLable.text = @"How to add my web site to Google Analytics?";
        _howTosViewController.contentToDisplay = CONTENT_ADDING_MORE_WEBSITES;
        [_howTosViewController reload];
        [_howTosViewController.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

-(IBAction)closeBtnAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        _howTosViewController.titleLable.text = @"";
        [_howTosViewController.closeBtn removeTarget:self action:@selector(closeBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    }];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
