//
//  WebProperties.h
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/13/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTLAnalytics.h"
#import "GTLServiceAnalytics.h"

@interface WebProperties : NSObject
{
    
}

@property(nonatomic, retain) GTLAnalyticsProfile *assignedProfile;
//stats
@property(nonatomic,assign)BOOL muted;
//for the selected period:
@property(nonatomic,assign)float visits;
@property(nonatomic,assign)float visitors;
@property(nonatomic,assign)float pageviews;
//

//Data corresponds to the currently selected period:
@property(nonatomic, retain) NSMutableArray *chartData, *countriesData;
@property(nonatomic,assign)int visitsFromBrowsers;
@property(nonatomic,assign)int visitsFromMobileDevices;

@end
