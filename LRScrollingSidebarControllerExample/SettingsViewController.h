//
//  SettingsViewController.h
//  MetricsApp
//
//  Created by Desi Nikolova on 3/27/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UISegmentedControl *_currentPeriod;
    IBOutlet UIButton *_btnSinceMonth, *_btnSinceYear;
    IBOutlet UITableView *_tableView;
    IBOutlet UIPickerView *_pickerView;
    IBOutlet UIButton *_doneBtn, *_cancelbtn;
    //
    NSMutableArray *_sinceMonthArray;
    NSMutableArray *_sinceYearArray;
    BOOL selectingSinceMonth;
    NSString *selectedRow;
}


-(void)reload;
-(NSString*)monthToNumber;
-(NSString*)yearToNumber;

@end
