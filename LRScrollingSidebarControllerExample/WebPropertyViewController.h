//
//  WebPropertyViewController.h
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/9/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProfileViewController;

@interface WebPropertyViewController : UIViewController<UIScrollViewDelegate>
{
    ProfileViewController *_currentProfile;
    IBOutlet UILabel *_currentProfleTitle;
    IBOutlet UIView *_activityView, *_activityBox;
    IBOutlet UILabel *_periodLabel, *_totalVisitsLabel;
}

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;
@property (nonatomic, strong) NSMutableArray *viewControllers;

-(void)loadUIWithData;
-(void)busyView;

@end
