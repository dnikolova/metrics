//
//  CountryData.m
//  Metrics
//
//  Created by Desi Nikolova on 3/31/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "CountryData.h"

@implementation CountryData

@synthesize countryName;
@synthesize visits;

-(id)initWithCountry:(NSString*)country andVisits:(int)numVisits;
{
    self = [super init];
    self.visits = numVisits;
    self.countryName = country;
    
    return self;
}

@end
