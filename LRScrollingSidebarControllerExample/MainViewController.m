// MainViewController.m


#import "MainViewController.h"
#import "MenuViewController.h"
#import "RightViewController.h"
#import "UIViewController+LRScrollingSidebarController.h"
#import "WebPropertyViewController.h"
#import "MoreMetricsViewController.h"

@implementation MainViewController


-(void)viewDidLoad
{
    [super viewDidLoad];
    //
    currentSection = SECTION_ANALYTICS;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    webPropertyViewController = [sb instantiateViewControllerWithIdentifier:@"WebPropertyViewController"];
    helpViewController = [sb instantiateViewControllerWithIdentifier:@"HelpViewController"];;
    settingsViewController  = [sb instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    educationViewController  = [sb instantiateViewControllerWithIdentifier:@"EducationViewController"];
    moreMetricsViewController = [sb instantiateViewControllerWithIdentifier:@"MoreMetricsViewController"];
    
    [self.view addSubview:webPropertyViewController.view];
    [self.view addSubview:helpViewController.view];
    [self.view addSubview:settingsViewController.view];
    [self.view addSubview:educationViewController.view];
    [self.view addSubview:moreMetricsViewController.view];

    [self updateView];
}

-(void)displaySection:(int)section
{
    currentSection = section;
    [self updateView];
}


-(void)updateView
{
    webPropertyViewController.view.hidden = YES;
    settingsViewController.view.hidden = YES;
    educationViewController.view.hidden = YES;
    helpViewController.view.hidden = YES;
    moreMetricsViewController.view.hidden = YES;
    
    switch (currentSection) {
        case SECTION_ANALYTICS:
        {
            //add the view and update the content;
            [webPropertyViewController loadUIWithData];
            webPropertyViewController.view.hidden = NO;
        }
            break;
        case SECTION_HELP:
        {
            //add the view and update the content;
            helpViewController.view.hidden = NO;
        }
            break;
        case SECTION_EDUCATION:
        {
            //add the view and update the content;
            educationViewController.view.hidden = NO;
        }
            break;
        case SECTION_MORE_METRICS:
        {
            //add the view and update the content;
            moreMetricsViewController.view.hidden = NO;
        }
            break;
        case SECTION_SETTINGS:
        {
            //add the view and update the content;
            settingsViewController.view.hidden = NO;
            [settingsViewController reload];
        }
            break;
        default:
            break;
    }
    
    [self.view bringSubviewToFront:menuButton];
}

- (IBAction)showLeftPanelButtonTapped:(id)sender
{
    [self.scrollingSidebarController showLeftViewControllerAnimated:YES];
}

- (IBAction)replaceLeftPanelButtonTapped:(id)sender
{
    MenuViewController *leftViewController = [[MenuViewController alloc] init];
    leftViewController.view.backgroundColor = LRGetRandomColor();
    [self.scrollingSidebarController showLeftViewController:leftViewController
                                                   animated:YES];
}

- (IBAction)showRightPanelButtonTapped:(id)sender
{
    [self.scrollingSidebarController showRightViewControllerAnimated:YES];
}

- (IBAction)replaceRightPanelButtonTapped:(id)sender
{
    RightViewController *rightViewController = [[RightViewController alloc] init];
    rightViewController.view.backgroundColor = LRGetRandomColor();
    [self.scrollingSidebarController showRightViewController:rightViewController
                                                    animated:YES];
}

- (NSArray *)mainPanelAvailableColors
{
    return @[
             [UIColor blueColor],
             [UIColor blackColor],
             [UIColor darkGrayColor],
             [UIColor magentaColor],
             [UIColor orangeColor],
             [UIColor purpleColor],
             [UIColor brownColor],
             ];
}

#pragma mark - LRScrollingSidebarController

- (void)controllerIsVisible:(BOOL)controllerIsVisible
{
    NSLog(@"Main view controller is visible: %d", controllerIsVisible);
}

@end
