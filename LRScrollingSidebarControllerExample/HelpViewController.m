//
//  HelpViewController.m
//  MetricsApp
//
//  Created by Desi Nikolova on 3/27/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    
    _howTosViewController = [sb instantiateViewControllerWithIdentifier:@"HowTosViewController"];
    _howTosViewController.titleLable.text = @"";
    [_howTosViewController.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(IBAction)clearingSession:(id)sender
{
    //to do - fill content
    [self presentViewController:_howTosViewController animated:YES completion:^{
        _howTosViewController.titleLable.text = @"Clearing current session";
        _howTosViewController.contentToDisplay = CONTENT_CLEARING_CURRENT_SESSION;
        [_howTosViewController reload];
         [_howTosViewController.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
    

}

-(IBAction)configuringPeriod:(id)sender
{
    //to do - fill content
    [self presentViewController:_howTosViewController animated:YES completion:^{
        _howTosViewController.titleLable.text = @"Configuring data period";
        _howTosViewController.contentToDisplay = CONTENT_CONFIGURING_DATA_PERIOD;
        [_howTosViewController reload];
         [_howTosViewController.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

-(IBAction)refreshingData:(id)sender
{
    //to do - fill content
    [self presentViewController:_howTosViewController animated:YES completion:^{
        _howTosViewController.titleLable.text = @"Refreshing data";
        _howTosViewController.contentToDisplay = CONTENT_REFRESHING_THE_DATA;
        [_howTosViewController reload];
        [_howTosViewController.closeBtn addTarget:self action:@selector(closeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    }];
}

-(IBAction)closeBtnAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        _howTosViewController.titleLable.text = @"";
        [_howTosViewController.closeBtn removeTarget:self action:@selector(closeBtnAction:)  forControlEvents:UIControlEventTouchUpInside];
    }];
}



@end
