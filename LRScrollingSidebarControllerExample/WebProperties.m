//
//  WebProperties.m
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/13/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "WebProperties.h"

@implementation WebProperties

@synthesize assignedProfile;
@synthesize visits, visitors, pageviews;
@synthesize muted;
@synthesize countriesData;
@synthesize visitsFromBrowsers, visitsFromMobileDevices;

-(id)init
{
    self = [super init];
    self.muted = NO;
    
    return self;
}

@end
