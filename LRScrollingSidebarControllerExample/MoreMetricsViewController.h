//
//  MoreMetricsViewController.h
//  Metrics
//
//  Created by Desi Nikolova on 5/11/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreMetricsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *_tableView;
    NSArray *metricsCategories;
    
}
@end
