//
//  ChartView.m
//  Metrics
//
//  Created by Desi Nikolova on 4/1/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "ChartView.h"
#import "AppSettings.h"
#import "ChartPoint.h"

#define kChartPadding 35

@implementation ChartView

@synthesize periodDaily;
@synthesize chartData;
@synthesize noDataLabel, visitsPeriodData;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 */

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, 1.0);//white
    CGContextSetLineWidth(context, 2.0);
    //
    
    //Moving through the data points for the selected period:
    CGAffineTransform trans = CGAffineTransformMakeScale(1, -1);
    
    CGContextSetTextMatrix(context, trans);
    
    CGContextMoveToPoint(context, kChartPadding, rect.size.height - kChartPadding);
    
    float xOffset = kChartPadding;
    float chartWidth = rect.size.width - kChartPadding * 2;
    float chartHei = rect.size.height - kChartPadding * 2;
    float step = chartWidth / [self xAxisStep];
    int intervals = [self xAxisIntervals];
    float maxVisit = [self maxVisitsNumber];
    float scaleFactor = 1.0;
    if(maxVisit > 0.0)
        scaleFactor = chartHei / maxVisit;
    for(int i = 0; i < [self xAxisStep] + 1; i++)
    {
        float yPos = rect.size.height - kChartPadding - [self getVisitsAtTime:i] * scaleFactor;// 22; //i.e. the number of visits for this time slot:
        NSLog(@"visit - ypos %f ( visits: %f )", yPos, [self getVisitsAtTime:i] );
        //scale down to the available space:
        if(yPos == 0.0)
            yPos = rect.size.height - kChartPadding;
        CGContextAddLineToPoint(context, xOffset, yPos);
        xOffset += step;
    }

    CGContextAddLineToPoint(context, kChartPadding + chartWidth, rect.size.height - kChartPadding);
    
    //Darwing the gradient
    
    UIColor *green = [UIColor colorWithRed:77.0/255.0 green:222.0/255.0 blue:77.0/255.0 alpha:.44];
    UIColor *cyan = [UIColor colorWithRed:66.0/255.0 green:179.0/255. blue:219.0/255. alpha:1.0];
    CGGradientRef gradient = [self CreateGradient:[UIColor clearColor] endColor:green];
    CGPoint start = CGPointMake( 150.0, 120.0);
    CGPoint end = CGPointMake( 150.0, rect.size.height - 0.0);
    CGGradientDrawingOptions options = 0;
    BOOL extendsPastStart = YES;
	if (extendsPastStart)
	{
		options |= kCGGradientDrawsBeforeStartLocation;
	}
	else
	{
		options |= kCGGradientDrawsAfterEndLocation;
	}
    
    
    CGContextClip(context);
    CGContextDrawLinearGradient(context, gradient, start, end, options);
    
    CGContextRestoreGState(context);
    CGContextDrawPath(context, kCGPathStroke);
    
    //Draw the contour:
    
    CGContextSetRGBStrokeColor(context, 77.0/255.0, 222.0/255.0, 77.0/255.0, .88);//green.
    CGContextSetLineWidth(context, 1.0);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    CGContextMoveToPoint(context, kChartPadding, rect.size.height - kChartPadding);
    xOffset = kChartPadding;
    for(int i = 0; i < [self xAxisStep] + 1; i++)
    {
        float yPos = rect.size.height - kChartPadding - [self getVisitsAtTime:i] * scaleFactor;// 22; //i.e. the number of visits for this time slot:
        NSLog(@"visit - ypos %f", yPos);
        //scale down to the available space:
        if(yPos == 0.0)
            yPos = rect.size.height - kChartPadding;
        CGContextAddLineToPoint(context, xOffset, yPos);
        xOffset += step;
    }
    CGContextAddLineToPoint(context, xOffset - step, rect.size.height - kChartPadding);
   // CGContextAddLineToPoint(context, 10.0, rect.size.height - 10.0);
    
    CGContextDrawPath(context, kCGPathStroke);
    
    //Draw circles for each data point - set transparent fill:
    
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, .55);
    CGContextSetRGBStrokeColor(context, 77.0/255.0, 222.0/255.0, 77.0/255.0, .88);
    
    //Configure the label formatting for each label:
    
    CGContextSetCharacterSpacing(context, 1.0);
    CGContextSetTextDrawingMode(context, kCGTextFill);
    CFStringRef string = NULL;
    CFStringRef fontName = CFSTR("Helvetica");;
    CTFontRef font = CTFontCreateWithName(fontName, 12.0, NULL);
    // Initialize the string, font, and context
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat components[] = { 1.0, 1.0, 1.0, 1.0 };
    
    CGColorRef white = CGColorCreate(rgbColorSpace, components);
    
    CGColorSpaceRelease(rgbColorSpace);
    CFStringRef keys[] = { kCTFontAttributeName, kCTForegroundColorAttributeName };
    CFTypeRef values[] = { font, white };
    
    CFDictionaryRef attributes =
    CFDictionaryCreate(kCFAllocatorDefault, (const void**)&keys,
                       (const void**)&values, sizeof(keys) / sizeof(keys[0]),
                       &kCFTypeDictionaryKeyCallBacks,
                       &kCFTypeDictionaryValueCallBacks);
    
    CGContextSetTextMatrix(context, trans);
    
    xOffset = kChartPadding;
    for(int i = 0; i < [self xAxisStep] + 1; i++)
    {
        float visits = [self getVisitsAtTime:i];
        float yPos = rect.size.height - kChartPadding - visits * scaleFactor;// 22; //i.e. the number of visits for this time slot:
        NSLog(@"visit - ypos %f", yPos);
        
        //scale down to the available space:
        BOOL drawLabel = YES;
        if(yPos == 0.0)
        {
            yPos = rect.size.height - kChartPadding;
            drawLabel = NO;
        }
        if(drawLabel)
        {
            string = CFStringCreateWithFormat(NULL, NULL, CFSTR("%i"), (int)visits);
            if(visits == 0)
                string = CFStringCreateWithFormat(NULL, NULL, CFSTR(" "));
            CFAttributedStringRef attrString =
            
            CFAttributedStringCreate(kCFAllocatorDefault, string, attributes);
            //C
            CTLineRef line = CTLineCreateWithAttributedString(attrString);
            // Set text position and draw the line into the graphics context
            CGContextSetTextPosition(context, xOffset - 10.0, yPos -  6.0);
            CTLineDraw(line, context);
            CFRelease(line);
        }
        //
        CGRect target = CGRectMake(xOffset - 4.0, yPos -  4.0, 8.0,8.0);
        CGContextAddEllipseInRect(context, target);
        CGContextStrokeEllipseInRect(context, target);
        CGContextFillEllipseInRect(context, target);
        xOffset += step;
    }
    
    //Draw the X/Y axis with labels:
    
    [self addAxis:context forRect:rect];
    
    //Update the period if necessary:
    
    NSString *period = @"Today";
    if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_THIS_MONTH)
    {
        period = @"This month";
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_MONTH)
    {
        period = [AppSettings instance].sinceMonth;
    }
    else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_YEAR)
    {
        period = [AppSettings instance].sinceYear;
    }
    visitsPeriodData.text = [NSString stringWithFormat:@"Visits %@", period];
}

-(void)addAxis:(CGContextRef)context forRect:(CGRect)rect
{
    //Setting the drawing properties:
    CGContextSetRGBStrokeColor(context, 1.0, 1.0, 1.0, .44);//white
    CGContextSetLineWidth(context, 2.0);
    
    CGContextMoveToPoint(context, kChartPadding, kChartPadding - 20.0);
    CGContextAddLineToPoint(context, kChartPadding, rect.size.height - kChartPadding);
    CGContextAddLineToPoint(context, 310.0, rect.size.height - kChartPadding);
    CGContextDrawPath(context, kCGPathStroke);
    
    //Draw the axis delimeters
    
    //X - Axis - time period
    CGContextSetCharacterSpacing(context, 1.0);
    CGContextSetTextDrawingMode(context, kCGTextFill);
    CFStringRef string = NULL;
    CFStringRef fontName = CFSTR("Helvetica");;
    CTFontRef font = CTFontCreateWithName(fontName, 14.0, NULL);
    // Initialize the string, font, and context
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGFloat components[] = { 1.0, 1.0, 1.0, 1.0 };
    
    CGColorRef white = CGColorCreate(rgbColorSpace, components);
    
    CGColorSpaceRelease(rgbColorSpace);
    CFStringRef keys[] = { kCTFontAttributeName, kCTForegroundColorAttributeName };
    CFTypeRef values[] = { font, white };
    
    CFDictionaryRef attributes =
    CFDictionaryCreate(kCFAllocatorDefault, (const void**)&keys,
                       (const void**)&values, sizeof(keys) / sizeof(keys[0]),
                       &kCFTypeDictionaryKeyCallBacks,
                       &kCFTypeDictionaryValueCallBacks);
    
    CGAffineTransform trans = CGAffineTransformMakeScale(1, -1);
    
    CGContextSetTextMatrix(context, trans);
    
        float xOffset = kChartPadding;
        float chartWidth = rect.size.width - kChartPadding * 2;
        float chartHei = rect.size.height - kChartPadding * 2;
        float step = chartWidth / [self xAxisStep];
        int intervals = [self xAxisIntervals];
       
        for(int i = 0; i < [self xAxisStep] + 1; i++)
        {
            CGContextMoveToPoint(context, xOffset, rect.size.height - kChartPadding - 2.5);
            CGContextAddLineToPoint(context, xOffset, rect.size.height - kChartPadding + 2.5);
            CGContextDrawPath(context, kCGPathStroke);
            
            //Label the days/months
            string = [self labelNameForIntervalOnXAxis:i];
            CFAttributedStringRef attrString =
            
            CFAttributedStringCreate(kCFAllocatorDefault, string, attributes);
            //C
            CTLineRef line = CTLineCreateWithAttributedString(attrString);
            // Set text position and draw the line into the graphics context
            CGContextSetTextPosition(context, xOffset - 5.0, rect.size.height - kChartPadding + 20.0);
            CTLineDraw(line, context);
            CFRelease(line);
            //
            xOffset += step;
        }
        
        // Y- Axis
        int maxVisits = (int)[self maxVisitsNumber];//find the maximum value;
        intervals = 7;
        float yOffset = kChartPadding;
        step = chartHei / intervals;
        int _visits_per_interval = (int) maxVisits / intervals;
        for(int i = 0; i < intervals; i++)
        {
            CGContextMoveToPoint(context, kChartPadding - 2.5, yOffset);
            CGContextAddLineToPoint(context, kChartPadding + 2.5, yOffset);
            CGContextDrawPath(context, kCGPathStroke);
            yOffset += step;
            
            ///Label the visits
            int visits = maxVisits - _visits_per_interval * i ;
            if(i == 0)
                visits = maxVisits;
            
            string = CFStringCreateWithFormat(NULL, NULL, CFSTR("%i"), visits);
            if(visits == 0)
                string = CFStringCreateWithFormat(NULL, NULL, CFSTR(" "));
            CFAttributedStringRef attrString =
            
            CFAttributedStringCreate(kCFAllocatorDefault, string, attributes);
            //C
            CTLineRef line = CTLineCreateWithAttributedString(attrString);
            // Set text position and draw the line into the graphics context
            CGContextSetTextPosition(context, kChartPadding - 22.0, yOffset - step);
            CTLineDraw(line, context);
            CFRelease(line);
        }
        
        CFRelease(string);
        CFRelease(attributes);
    
    UIImage *xAxisArrow = [UIImage imageNamed:@"arrow_x_axis.png"];
    CGRect imageRect = CGRectMake(rect.size.width - 17, rect.size.height - kChartPadding - 7.5, 15, 15);
    CGContextDrawImage(context, imageRect, xAxisArrow.CGImage);
    //
    UIImage *yAxisArrow = [UIImage imageNamed:@"arrow_y_axis.png"];
    imageRect = CGRectMake(kChartPadding - 7.5 , 5, 15, 15);
    CGContextDrawImage(context, imageRect, yAxisArrow.CGImage);
    //

}

-(CGGradientRef)CreateGradient:(UIColor*)startColor endColor:(UIColor*)endColor
{
    CGGradientRef result;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[2] = {1.0f, 0.0f};
    CGFloat startRed, startGreen, startBlue, startAlpha;
    CGFloat endRed, endGreen, endBlue, endAlpha;
    
    [endColor getRed:&endRed green:&endGreen blue:&endBlue alpha:&endAlpha];
    [startColor getRed:&startRed green:&startGreen blue:&startBlue alpha:&startAlpha];
    
    CGFloat componnents[8] = {
        startRed, startGreen, startBlue, startAlpha,
        endRed, endGreen, endBlue, endAlpha
    };
    result = CGGradientCreateWithColorComponents(colorSpace, componnents, locations, 2);
    CGColorSpaceRelease(colorSpace);
    return result;
}

-(int)xAxisIntervals
{
    if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_TODAY)
    {
        return 24;
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_THIS_MONTH)
    {
        return 30;
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_MONTH)
    {
        return 12;
    }
    else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_YEAR)
    {
        int sinceYear = [[AppSettings instance] yearToNumber].intValue;
        int thisYear = 2014;//to do
        return thisYear - sinceYear;
    }

    return 12;
}

-(int)xAxisStep
{
    if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_TODAY)
    {
        return 24/2;
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_THIS_MONTH)
    {
        return 30/3;
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_MONTH)
    {
        return 12;
    }
    else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_YEAR)
    {
        int sinceYear = [[AppSettings instance] yearToNumber].intValue;
        int thisYear = 2014;//to do
        return thisYear - sinceYear;
    }
    
    return 12;
}

-(int)yAxisIntervals
{
    return 1;
}


-(CFStringRef)labelNameForIntervalOnXAxis:(int)interval
{
    int intervalVal = 1;
    if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_TODAY)
    {
        intervalVal =  2 * interval;
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_THIS_MONTH)
    {
        intervalVal =  3 * interval;
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_MONTH)
    {
        intervalVal = interval;
    }
    else {
        int sinceYear = [[AppSettings instance] yearToNumber].intValue;
        int thisYear = 2014;//to do
        intervalVal = sinceYear + interval;// * (thisYear - sinceYear);
        return CFStringCreateWithFormat(NULL, NULL, CFSTR("%i"), intervalVal);
    }
    
    return CFStringCreateWithFormat(NULL, NULL, CFSTR("%i"), intervalVal);
}

-(float)getVisitsAtTime:(int)time
{
    if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_TODAY)
    {
        //24 hours:
        if(chartData.count > time)
        {
            ChartPoint *point = [chartData objectAtIndex:time];
            return point.value.floatValue;
        }
        
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_THIS_MONTH)
    {
        //30 days:
        if(chartData.count > time)
        {
            ChartPoint *point = [chartData objectAtIndex:time];
            return point.value.floatValue;
        }
        
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_MONTH)
    {
        //the array should be up to 12 months:
        if(chartData.count > time)
        {
            ChartPoint *point = [chartData objectAtIndex:time];
            return point.value.floatValue;
        }
    }
    else {
        //years
        if(chartData.count > time)
        {
            ChartPoint *point = [chartData objectAtIndex:time];
            return point.value.floatValue;
        }
    }
    return 0.0;
}

-(float)maxVisitsNumber
{
    float max = 0;
    for(ChartPoint *point in self.chartData)
    {
        if(point.value.floatValue > max)
            max = point.value.floatValue;
    }
    
    noDataLabel.hidden = (max == 0) ? NO : YES;
        
    return max;
}


@end
