//
//  PieChartView.h
//  Metrics
//
//  Created by Desi Nikolova on 4/27/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import "WebProperties.h"

@interface PieChartView : UIView
{
    NSMutableArray *angles;
    BOOL otherCountriesSector;
    NSArray *sectorColors; //12.
}

@property(nonatomic, strong) WebProperties *assignedProperty;
@property(nonatomic, assign) BOOL dataByCountry;

//Utils:

CGPoint PointOnCircle(float radius, float angleInDegrees, CGPoint origin);

-(void)computeAngles;

@end
