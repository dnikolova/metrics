//
//  AppSettings.m
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/13/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "AppSettings.h"

@implementation AppSettings

@synthesize analyticsChartPeriod;
@synthesize analyticsChartType;
@synthesize periodOptions;
@synthesize sinceMonth, sinceYear;
@synthesize pieChartColors;
@synthesize pieChartBorderColor,axisAndIndicatorsColor;

static AppSettings *m_instance = nil;

+(AppSettings*)instance
{
    if(!m_instance)
        m_instance = [[AppSettings alloc] init];
    
    return m_instance;
}

-(id)init
{
    self = [super init];
    
    analyticsChartType = CHART_THIS_MONTH;
    periodOptions = [[NSMutableArray alloc] initWithObjects:@"Today",@"This Month",@"Since Jan",@"Since 2011", nil];
    sinceMonth = @"Since Jan";
    sinceYear = @"Since 2011";
    analyticsChartPeriod = PERIOD_IS_SINCE_YEAR;//starts with 2011 until saving to userdefaults here.
    
    [self loadColors];
    
    return self;
}

-(void)loadColors
{
    if(pieChartColors)
        return;
    
    NSLog(@"instantiating chart colors");
    
    pieChartBorderColor = [UIColor colorWithRed:77.0/255.0 green:222.0/77.0 blue:89.0/255.0 alpha:1.0];
    axisAndIndicatorsColor = [UIColor whiteColor];
    
    //153.0/255.0 156.0/255.0 156.0/255.0
    UIColor *color1 = [UIColor colorWithRed:77.0/255.0 green:222.0/77.0 blue:89.0/255.0 alpha:.55];
    //156.0/255.0       51.0/255.0       100.0/255.0
    UIColor *color2 = [UIColor colorWithRed:39.0/255.0 green:156.0/255.0 blue:207.0/255.0 alpha:1.0];
    //255.0/255.0    253.0/255.0  209.0/255.0
    UIColor *color3 = [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0];
    //201.0/255.0    255.0/255.0  255.0/255.0
    UIColor *color4 = [UIColor colorWithRed:255.0/255.0 green:128.0/255.0 blue:0.0/255.0 alpha:1.0];
    //102.0/255.0    4.0/255.0    95.0/255.0
    UIColor *color5 = [UIColor colorWithRed:50.0/255.0 green:200.0/255.0 blue:110.0/255.0 alpha:1.0];
    //255.0/255.0    128.0/255.0  130.0/255.0
    UIColor *color6 = [UIColor colorWithRed:255.0/255.0 green:128.0/255.0 blue:130.0/255.0 alpha:1.0];
    //0.0/255.0      105.0/255.0  196.0/255.0
    UIColor *color7 = [UIColor colorWithRed:0.0/255.0 green:105.0/255.0 blue:196.0/255.0 alpha:1.0];
    //205.0/255.0    206.0/255.0  250.0/255.0
    UIColor *color8 = [UIColor colorWithRed:205.0/255.0 green:206.0/255.0 blue:250.0/255.0 alpha:1.0];
    //2.0/255.0      10.0/255.0   120.0/255.0
    UIColor *color9 = [UIColor colorWithRed:2.0/255.0 green:10.0/255.0 blue:120.0/255.0 alpha:1.0];
    //255.0/255.0    25.0/255.0   244.0/255.0
    UIColor *color10 = [UIColor colorWithRed:255.0/255.0 green:25.0/255.0 blue:244.0/255.0 alpha:1.0];
    //255.0/255.0    252.0/255.0  87.0/255.0
    UIColor *color11 = [UIColor colorWithRed:255.0/255.0 green:252.0/255.0 blue:87.0/255.0 alpha:1.0];
    //0.0/255.0      255.0/255.0  251.0/255.0
    UIColor *color12 = [UIColor colorWithRed:0.0/255.0 green:255.0/255.0 blue:251.0/255.0 alpha:1.0];
    
    pieChartColors = [[NSMutableArray alloc] initWithObjects:color1,color2,color3,color4,color5,color6,color7,color8,color9,color10,color11,color12, nil];
    
    NSLog(@"num of colors %i", pieChartColors.count);
    
    
}


-(NSString*)monthToNumber
{
    NSArray *array = [sinceMonth componentsSeparatedByString:@"Since "];
    NSString *month = [array lastObject];
    
    //Jan",@"Feb",@"March",@"April",@"May",@"June",@"July",@"Aug",@"Sept",@"Oct",@"Nov",@"Dec"
    //
    
    if([month isEqualToString:@"Jan"] || [month isEqualToString:@"January"])
    {
        return @"01";
    }else if([month isEqualToString:@"Feb"])
    {
      return @"02";
    }else if([month isEqualToString:@"March"])
    {
        return @"03";
    }else if([month isEqualToString:@"April"])
    {
        return @"04";
    }else if([month isEqualToString:@"May"])
    {
        return @"05";
    }else if([month isEqualToString:@"June"])
    {
        return @"06";
    }else if([month isEqualToString:@"July"])
    {
        return @"07";
    }else if([month isEqualToString:@"Aug"])
    {
        return @"08";
    }else if([month isEqualToString:@"Sept"])
    {
        return @"09";
    }else if([month isEqualToString:@"Oct"])
    {
        return @"10";
    }else if([month isEqualToString:@"Nov"])
    {
        return @"11";
    }else if([month isEqualToString:@"Dec"])
    {
        return @"12";
    }
    
    return @"01";
}

-(NSString*)yearToNumber
{
    NSArray *array = [sinceYear componentsSeparatedByString:@"Since "];
    return [array lastObject];
}

@end
