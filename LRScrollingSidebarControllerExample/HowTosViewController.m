//
//  HowTosViewController.m
//  Metrics
//
//  Created by Desi Nikolova on 3/30/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "HowTosViewController.h"

@interface HowTosViewController ()

@end

@implementation HowTosViewController

@synthesize titleLable, closeBtn;
@synthesize contentToDisplay;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    configureView.hidden = YES;
    refreshingData.hidden = YES;
    clearSession.hidden = YES;
    monitoringDataView.hidden = YES;
    //
    // Do any additional setup after loading the view.
    monitoringYourData = @"Your data is a valuable asset. Monitoring the data your web site generates is a guide to you and your business partners. Now it is easier than ever to plan and execute your strategy based on real time analytics, keep track of your web site visitors, the frequency of their visits, and their location. With Metrics you can do all of this for free, from everywhere, and at any time.";
    
    addingMoreWebSites = @"Google Analytics provides the option to monitor your web site data in a  user friendly way. You can always start monitoring a web site by including it as a property from your Google Analytics account.Simply navigate to Google Analytics after logging to your Google account.........Start monitoring. After refreshing the data in Metrics, you will see your new domain in the 'My websites' data' section.";
    
    usingGoogleAnalyticsOnline = @"Main sections with purposes";// ..
    
    configuringDataPeriod = @"You can configure the data period from the Settings section. To customize the 'Since month' and 'Since year' options, just tap on the green labels indicating the currently selected year and month. Your choice will automatically show up in the 'My Web site data' section.";// Pic.
    
    clearingCurrentSession = @"To clear your current session simply sign out – this will remove your security token and the permissions you granted to Metrics. Logging back will generate a new token and prompt you to grant permission to your account again. This will generate a new security token and allow Metrics an access to your data.";//Pic.. (arrow pointing to the button).
    
    refreshingTheData = @"You can refresh the data from the settings' section or each individual web site view.";//Pic..
}

-(void)reload
{
    /*
     CONTENT_MONITORING_YOUR_DATA,//additional
     CONTENT_ADDING_MORE_WEBSITES,//additional
     CONTENT_USING_GOOGLE_ANALYTICS,//additional
     //
     CONTENT_CONFIGURING_DATA_PERIOD,//how tos
     CONTENT_CLEARING_CURRENT_SESSION,//how tos
     CONTENT_REFRESHING_THE_DATA
     */

    configureView.hidden = YES;
    clearSession.hidden = YES;
    refreshingData.hidden = YES;
    monitoringDataView.hidden = YES;
    addMoreWebsitesView.hidden = YES;
    usingGoogleView.hidden = YES;
    
    switch (contentToDisplay) {
            //Additional info
        case CONTENT_MONITORING_YOUR_DATA:
        {
            monitoringDataView.hidden = NO;
        }
            break;
        case CONTENT_ADDING_MORE_WEBSITES:
        {
            //
            addMoreWebsitesView.hidden = NO;
        }
            break;
        case CONTENT_USING_GOOGLE_ANALYTICS:
        {
            //
            usingGoogleView.hidden = NO;
        }
            break;
            //How tos
        case CONTENT_CONFIGURING_DATA_PERIOD:
        {
            configureView.hidden = NO;
        }
            break;
        case CONTENT_CLEARING_CURRENT_SESSION:
        {
            clearSession.hidden = NO;
        }
            break;
        case CONTENT_REFRESHING_THE_DATA:
        {
            refreshingData.hidden = NO;
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
