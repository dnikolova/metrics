//
//  AppSettings.h
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/13/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
  CHART_THIS_MONTH,
  CHART_THIS_YEAR
}ChartType;


enum {
    PERIOD_IS_TODAY = 0,
    PERIOD_IS_THIS_MONTH,
    PERIOD_IS_SINCE_MONTH,
    PERIOD_IS_SINCE_YEAR
}PeriodType;

@interface AppSettings : NSObject

@property(nonatomic, retain) NSMutableArray *periodOptions;
@property(nonatomic, retain) NSString *sinceMonth, *sinceYear;
@property(nonatomic, assign) int analyticsChartPeriod;
@property(nonatomic, assign) int analyticsChartType;
//Pie & Graph chart configuration:
@property(nonatomic, strong) NSMutableArray *pieChartColors; 
@property(nonatomic, strong) UIColor *pieChartBorderColor;
@property(nonatomic, strong) UIColor *axisAndIndicatorsColor;

+(AppSettings*)instance;
-(NSString*)monthToNumber;
-(NSString*)yearToNumber;
-(void)loadColors;

@end
