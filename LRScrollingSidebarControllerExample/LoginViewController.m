//
//  LoginViewController.m
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/4/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "LoginViewController.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTMOAuth2SignIn.h"
#import "AppDelegate.h"
#import "GTLAnalytics.h"
#import "GTLServiceAnalytics.h"
#import "DataManager.h"

static NSString *const kKeychainItemName = @"OAuth Sample: Google Contacts";
static NSString *const kShouldSaveInKeychainKey = @"shouldSaveInKeychain";

static NSString *const kDailyMotionAppServiceName = @"OAuth Sample: DailyMotion";
static NSString *const kDailyMotionServiceName = @"DailyMotion";

static NSString *const kSampleClientIDKey = @"clientID";
static NSString *const kSampleClientSecretKey = @"clientSecret";


@interface LoginViewController ()
- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error;
- (void)incrementNetworkActivity:(NSNotification *)notify;
- (void)decrementNetworkActivity:(NSNotification *)notify;
- (void)signInNetworkLostOrFound:(NSNotification *)notify;
- (GTMOAuth2Authentication *)authForDailyMotion;
- (void)doAnAuthenticatedAPIFetch;
- (void)displayAlertWithMessage:(NSString *)str;
- (BOOL)shouldSaveInKeychain;
- (void)saveClientIDValues;
- (void)loadClientIDValues;

@end

@implementation LoginViewController

@synthesize auth = mAuth;

// NSUserDefaults keys
static NSString *const kGoogleClientIDKey          = @"GoogleClientID";
static NSString *const kGoogleClientSecretKey      = @"GoogleClientSecret";
static NSString *const kDailyMotionClientIDKey     = @"DailyMotionClientID";
static NSString *const kDailyMotionClientSecretKey = @"DailyMotionClientSecret";

#define kClientIDValue @"720278981447-eb7mns6h28gltaea81v0e7qh7fjcnmjf.apps.googleusercontent.com"
#define kClientSecretValue @"29jZtlAPM_IdnSDKRUaVWRc6"

- (void)awakeFromNib
{
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Listen for network change notifications
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(incrementNetworkActivity:) name:kGTMOAuth2WebViewStartedLoading object:nil];
    [nc addObserver:self selector:@selector(decrementNetworkActivity:) name:kGTMOAuth2WebViewStoppedLoading object:nil];
    [nc addObserver:self selector:@selector(incrementNetworkActivity:) name:kGTMOAuth2FetchStarted object:nil];
    [nc addObserver:self selector:@selector(decrementNetworkActivity:) name:kGTMOAuth2FetchStopped object:nil];
    [nc addObserver:self selector:@selector(signInNetworkLostOrFound:) name:kGTMOAuth2NetworkLost  object:nil];
    [nc addObserver:self selector:@selector(signInNetworkLostOrFound:) name:kGTMOAuth2NetworkFound object:nil];
    
    // First, we'll try to get the saved Google authentication, if any, from
    // the keychain
    
    // Normal applications will hardcode in their client ID and client secret,
    // but the sample app allows the user to enter them in a text field, and
    // saves them in the preferences
    NSString *clientID = kClientIDValue;
    NSString *clientSecret = kClientSecretValue;
    
    GTMOAuth2Authentication *auth = nil;
    
    if (clientID && clientSecret) {
        auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                     clientID:clientID
                                                                 clientSecret:clientSecret];
       // NSLog(@"OAuth info - %@ - %@ - %@", auth.userEmail, auth.userData, auth.userEmailIsVerified);
        if(!auth.userEmailIsVerified || signedOut)
        {
            signInBtn.hidden = NO;
            //
        
        }else{
            
            signedOut = NO;
            self.auth = auth;
            
            [self performSelector:@selector(updateUI) withObject:nil afterDelay:.44];
            
            [self analyticsQuery];
            
            return;
        }
         
    }else{
        //NSLog(@"OAuth is nil!");
    }
    
    // Save the authentication object, which holds the auth tokens and
    // the scope string used to obtain the token.  For Google services,
    // the auth object also holds the user's email address.
    self.auth = auth;
    
    BOOL isRemembering = [self shouldSaveInKeychain];
   
    NSLog(@"is the session stored? %d", isRemembering);
	
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)showLoginBtn
{
    signInBtn.hidden = NO;
    signInBtn.alpha = 1.0;
}

-(IBAction)showSignIn:(id)sender
{
    
    [self signInToGoogle];
    return;
    //[self.navigationController pushViewController:_signinViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* Auth */

- (void)signInToGoogle
{
    /* Stay signed in (by default) - use the method below to revoke the token! */
    //[self signOut];
    
    BOOL isRemembering = [self shouldSaveInKeychain];
    if(!isRemembering)
    {
        [self registerInKeychain];
        isRemembering = [self shouldSaveInKeychain];
    }
    
    
    NSLog(@"is the session stored? %d", isRemembering);
	
    
    NSString *keychainItemName = nil;
    if ([self shouldSaveInKeychain])
    {
        keychainItemName = kKeychainItemName;
    }
    
    // For Google APIs, the scope strings are available
    // in the service constant header files.
    NSString *scope = @"https://www.googleapis.com/auth/analytics.readonly"; //@"https://www.googleapis.com/auth/plus.me";
    
    // Typically, applications will hardcode the client ID and client secret
    // strings into the source code; they should not be user-editable or visible.
    //
    // But for this sample code, they are editable.
    NSString *clientID = kClientIDValue;
    NSString *clientSecret = kClientSecretValue;
    
    if ([clientID length] == 0 || [clientSecret length] == 0) {
        NSString *msg = @"The sample code requires a valid client ID and client secret to sign in.";
        [self displayAlertWithMessage:msg];
        return;
    }
    
    // Note:
    // GTMOAuth2ViewControllerTouch is not designed to be reused. Make a new
    // one each time you are going to show it.
    
    // Display the autentication view.
    SEL finishedSel = @selector(viewController:finishedWithAuth:error:);
    
    GTMOAuth2ViewControllerTouch *viewController;
    viewController = [GTMOAuth2ViewControllerTouch controllerWithScope:scope
                                                              clientID:clientID
                                                          clientSecret:clientSecret
                                                      keychainItemName:keychainItemName
                                                              delegate:self
                                                      finishedSelector:finishedSel];
    
    // You can set the title of the navigationItem of the controller here, if you
    // want.
    
    // If the keychainItemName is not nil, the user's authorization information
    // will be saved to the keychain. By default, it saves with accessibility
    // kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly, but that may be
    // customized here. For example,
    //
    //   viewController.keychainItemAccessibility = kSecAttrAccessibleAlways;
    
    // During display of the sign-in window, loss and regain of network
    // connectivity will be reported with the notifications
    // kGTMOAuth2NetworkLost/kGTMOAuth2NetworkFound
    //
    // See the method signInNetworkLostOrFound: for an example of handling
    // the notification.
    
    // Optional: Google servers allow specification of the sign-in display
    // language as an additional "hl" parameter to the authorization URL,
    // using BCP 47 language codes.
    //
    // For this sample, we'll force English as the display language.
    NSDictionary *params = [NSDictionary dictionaryWithObject:@"en"
                                                       forKey:@"hl"];
    viewController.signIn.additionalAuthorizationParameters = params;
    
    // By default, the controller will fetch the user's email, but not the rest of
    // the user's profile.  The full profile can be requested from Google's server
    // by setting this property before sign-in:
    //
    //   viewController.signIn.shouldFetchGoogleUserProfile = YES;
    //
    // The profile will be available after sign-in as
    //
    //   NSDictionary *profile = viewController.signIn.userProfile;
    
    // Optional: display some html briefly before the sign-in page loads
    NSString *html = @"<html><body bgcolor=silver><div align=center>Loading sign-in page...</div></body></html>";
    viewController.initialHTMLString = html;
    
    [[self navigationController] pushViewController:viewController animated:YES];
    
    // The view controller will be popped before signing in has completed, as
    // there are some additional fetches done by the sign-in controller.
    // The kGTMOAuth2UserSignedIn notification will be posted to indicate
    // that the view has been popped and those additional fetches have begun.
    // It may be useful to display a temporary UI when kGTMOAuth2UserSignedIn is
    // posted, just until the finished selector is invoked.
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
    if (error != nil) {
        // Authentication failed (perhaps the user denied access, or closed the
        // window before granting access)
        NSLog(@"Authentication error: %@", error);
        NSData *responseData = [[error userInfo] objectForKey:@"data"]; // kGTMHTTPFetcherStatusDataKey
        if ([responseData length] > 0) {
            // show the body of the server's authentication failure response
            NSString *str = [[NSString alloc] initWithData:responseData
                                                   encoding:NSUTF8StringEncoding];
            NSLog(@"%@", str);
        }
        
        self.auth = nil;
        
        //alert for the error;
    } else {
        // Authentication succeeded
        //
        // At this point, we either use the authentication object to explicitly
        // authorize requests, like
        //
        //  [auth authorizeRequest:myNSURLMutableRequest
        //       completionHandler:^(NSError *error) {
        //         if (error == nil) {
        //           // request here has been authorized
        //         }
        //       }];
        //
        // or store the authentication object into a fetcher or a Google API service
        // object like
        //
        GTMHTTPFetcher *fetcher = [[GTMHTTPFetcher alloc] init];
        [fetcher setAuthorizer:auth];
        
        // save the authentication object
        self.auth = auth;
        _activityView.hidden = NO;
        
        [self performSelector:@selector(updateUI) withObject:nil afterDelay:.44];
        
        [self analyticsQuery];
    }
    
}

/* Start data retrieval */

- (void)analyticsQuery
{
    [DataManager instance].auth = self.auth;
    [[DataManager instance] analyticsQuery];
}


-(void)updateUI
{
    NSLog(@"login and show the table");
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate loginSuccess];
}


- (void)displayAlertWithMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Authentication error"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
    [alert show];
}


- (void)signOut {
    if ([self.auth.serviceProvider isEqual:kGTMOAuth2ServiceProviderGoogle]) {
        // remove the token from Google's servers
        [GTMOAuth2ViewControllerTouch revokeTokenForGoogleAuthentication:self.auth];
    }
    
    // remove the stored Google authentication from the keychain, if any
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
    
    // remove the stored DailyMotion authentication from the keychain, if any
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kDailyMotionAppServiceName];
    
    // Discard our retained authentication object.
    self.auth = nil;
    
    signedOut = YES;
    
}


- (BOOL)shouldSaveInKeychain {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL flag = [defaults boolForKey:kShouldSaveInKeychainKey];
    return flag;
}

-(void)registerInKeychain
{
   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:kShouldSaveInKeychainKey];
}

#pragma mark Client ID and Secret

//
// Normally an application will hardwire the client ID and client secret
// strings in the source code.  This sample app has to allow them to be
// entered by the developer, so we'll save them across runs into preferences.
//

- (void)saveClientIDValues {
    // Save the client ID and secret from the text fields into the prefs
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *clientID = kClientIDValue;
    NSString *clientSecret = kClientSecretValue;
    
    [defaults setObject:clientID forKey:kGoogleClientIDKey];
    [defaults setObject:clientSecret forKey:kGoogleClientSecretKey];
    
}

- (void)loadClientIDValues {
    // Load the client ID and secret from the prefs into the text fields
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
}

#pragma mark -

- (void)incrementNetworkActivity:(NSNotification *)notify {
    ++mNetworkActivityCounter;
    if (mNetworkActivityCounter == 1) {
        UIApplication *app = [UIApplication sharedApplication];
        [app setNetworkActivityIndicatorVisible:YES];
    }
}

- (void)decrementNetworkActivity:(NSNotification *)notify {
    --mNetworkActivityCounter;
    if (mNetworkActivityCounter == 0) {
        UIApplication *app = [UIApplication sharedApplication];
        [app setNetworkActivityIndicatorVisible:NO];
    }
}

- (void)signInNetworkLostOrFound:(NSNotification *)notify {
    if ([[notify name] isEqual:kGTMOAuth2NetworkLost]) {
        // network connection was lost; alert the user, or dismiss
        // the sign-in view with
        //   [[[notify object] delegate] cancelSigningIn];
    } else {
        // network connection was found again
    }
}

#pragma mark -



@end
