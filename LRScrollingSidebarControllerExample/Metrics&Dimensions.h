//
//  Metrics&Dimensions.h
//  Metrics
//
//  Created by Desi Nikolova on 3/31/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#ifndef Metrics_Metrics_Dimensions_h
#define Metrics_Metrics_Dimensions_h



/* Metrics and Dimensions Macros */

#define kGAVisits @"ga:visits"
#define kGAVisitors @"ga:visitors"
#define kGAPageviews @"ga:pageviews"

/* Platform related - all are dimensions
 
 ga:browser
 ga:browserVersion
 ga:operatingSystem
 ga:operatingSystemVersion
 ga:isMobile
 ga:isTablet
 ga:mobileDeviceBranding
 ga:mobileDeviceModel
 ga:mobileInputSelector
 ga:mobileDeviceInfo
 ga:mobileDeviceMarketingName
 ga:deviceCategory
 
 */

//Agent platform dimensions
#define kGADeviceCategory @"ga:deviceCategory"// - The type of device: Desktop, Tablet, or Mobile.
#define kGADeviceIsMobile @"ga:isMobile" // - Marketing name used for mobile device.

//In-app purchases dimensions:


/*
 Audience
 
 Dimensions
 
 ga:userAgeBracket
 ga:visitorAgeBracket
 ga:userGender
 ga:visitorGender
 */

#define kGAVisitorAgeBracket @"ga:visitorAgeBracket"
#define kGAVisitorGender @"ga:visitorGender"

/*
 Adsense:
 
 Metrics
 
 ga:adsenseRevenue
 ga:adsenseAdUnitsViewed
 ga:adsenseAdsViewed
 ga:adsenseAdsClicks
 
*/

#define kGAAdsenseRevenue @"ga:adsenseRevenue"
#define kGAAdsenseAdUnitsViewed @"ga:adsenseAdUnitsViewed"
#define kGAAdsenseAdsViewed @"ga:adsenseAdsViewed"
#define kGAAdsenseAdsClicks @"ga:dsenseAdsClicks"

/*
 Geo Network

 ga:city
 ga:region
 ga:latitude
 ga:longitude
 ga:networkDomain
 ga:networkLocation
 */

#define kGACity @"ga:city"
#define kGARegion @"ga:region"
#define kGALatitude @"ga:latitude"
#define kGALongitude @"ga:longitude"
#define kGANetworkDomain @"ga:networkDomain"
#define kGANetworkLocation @"ga:networkLocation"

/*
 
 Platform or Device
 ga:mobileDeviceBranding
 */

#define kGAMobileDeviceBranding @"ga:mobileDeviceBranding"

/*

 System
 
 ga:flashVersion
 ga:javaEnabled
 ga:language
 */

#define kGAFlashVersion @"ga:flashVersion"
#define kGAJavaEnabled @"ga:javaEnabled"
#define kGALanguage @"ga:language"

/*
 Exceptions
 
 Metrics
 ga:exceptions
 ga:fatalExceptions
 
 */

#define kGAExceptions @"ga:exceptions"
#define kGAFatalExceptions @"ga:fatalExceptions"

//Preloaded - coming at the start:

#define kGADeviceTablet @"ga:isTablet"// - Indicates tablet visitors. The possible values are Yes or No.
#define kGABrowser @"ga:browser"//i.e. IE, FireFox, Chrome, etc.



//Agent platform dimensions
#define kGADCountry @"ga:country"//

//Add these as in-app purchases:
#define kGACity @"ga:city"
#define kGALatitude @"ga:latitude"
#define kGALongitude @"ga:longitude"

////////////////////////////////////



#endif
