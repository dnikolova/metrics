//
//  ProfileViewController.h
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/15/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CPTGraphHostingView.h"
#import "_CPTStocksTheme.h"
#import "_CPTSlateTheme.h"
#import "WebProperties.h"
#import "GradientScatterPlot.h"
#import "PlotGallery.h"
#import "CPTGraphHostingView.h"
#import "CPTTheme.h"
#import "MetricsDetailedViewController.h"
#import "ChartView.h"

@class WebPropertyViewController;

@interface ProfileViewController : UIViewController
{
    IBOutlet UILabel *visitsToday, *visitsYesterdayPercent;
    IBOutlet UILabel *visitorsToday, *visitorsYesterdayPercent;
    IBOutlet UILabel *pageviewsToday, *pageviewsYesterdayPercent;
    //
    IBOutlet UILabel *titleForPeriod;
    //
    MetricsDetailedViewController *_detailsViewCotroller;
    
    IBOutlet UISegmentedControl *_periodsSegmControl;
}

@property(nonatomic, retain) WebProperties *assignedProperty;
@property(nonatomic, retain) IBOutlet CPTGraphHostingView *plotRenderView;
@property(nonatomic, retain) CPTGraphHostingView *hostingView;
@property(nonatomic, retain) GradientScatterPlot *plot;
@property(nonatomic, retain) CPTTheme *_theme;
@property(nonatomic, retain) IBOutlet ChartView *chartView;
@property(nonatomic, retain) WebPropertyViewController *parentView;

-(void)updateUI;

@end
