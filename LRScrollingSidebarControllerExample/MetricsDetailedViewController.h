//
//  MetricsDetailedViewController.h
//  MetricsApp
//
//  Created by Desi Nikolova on 3/26/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebProperties.h"
#import "PieChartView.h"

@interface MetricsDetailedViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *_tableView;
    IBOutlet PieChartView *_pieChartView;
    IBOutlet UIActivityIndicatorView *_activity;
    IBOutlet UILabel *selectedPeriodLabel, *totalVisitsLabel;
    IBOutlet UIView *loadingView;
}

@property(nonatomic, assign) BOOL dataByCountry;
@property(nonatomic, retain) WebProperties *assignedProfile;

@end
