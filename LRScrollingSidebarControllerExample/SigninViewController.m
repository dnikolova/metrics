//
//  SigninViewController.m
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/4/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "SigninViewController.h"
#import "AppDelegate.h"

@interface SigninViewController ()

@end

@implementation SigninViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)login:(id)sender
{
    [self performSelector:@selector(showMenu) withObject:nil afterDelay:.44];
}

-(void)showMenu
{
    NSLog(@"login and show the table");
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate loginSuccess];
}

@end
