//
//  MoreMetricsViewController.m
//  Metrics
//
//  Created by Desi Nikolova on 5/11/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "MoreMetricsViewController.h"
#import "DataManager.h"

@interface MoreMetricsViewController ()

@end

@implementation MoreMetricsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView
{
    [super loadView];
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tableView.delegate = self;
    _tableView.dataSource = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSMutableDictionary *dict = [[DataManager instance].inAppPurchasesMoreMetrics objectAtIndex:section];
    NSMutableArray *items = [dict objectForKey:@"metrics"];
    
    return items.count;//
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [DataManager instance].inAppPurchasesMoreMetrics.count;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSMutableDictionary *dict = [[DataManager instance].inAppPurchasesMoreMetrics objectAtIndex:section];
    
    return [dict objectForKey:@"title"];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.0,0.0,320.0, 44.0)];
    header.backgroundColor = [UIColor colorWithRed:.1 green:.1 blue:.1 alpha:.1];
    NSMutableDictionary *dict = [[DataManager instance].inAppPurchasesMoreMetrics objectAtIndex:section];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0.0,5.0,_tableView.frame.size.width, 34.0)];
    title.textAlignment = UITextAlignmentCenter;
    title.text = [dict objectForKey:@"title"];
    title.font = [UIFont boldSystemFontOfSize:18];
    title.textColor = [UIColor whiteColor];
    //title.backgroundColor = [UIColor greenColor];
    [header addSubview:title];
    
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MoreMetricsCell";
	
	// Dequeue or create a cell of the appropriate type.
	UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSMutableDictionary *dict = [[DataManager instance].inAppPurchasesMoreMetrics objectAtIndex:indexPath.section];
    NSMutableArray *items = [dict objectForKey:@"metrics_names"];
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dataIcon.png"]];
        icon.tag = 88;
        //[cell.contentView addSubview:icon];
        
        CGRect frame = icon.frame;
        frame.origin = CGPointMake(12, 10);
        frame.size = CGSizeMake(CGImageGetWidth(icon.image.CGImage) / 2, CGImageGetHeight(icon.image.CGImage) / 2);
        icon.frame = frame;
        //icon.contentMode = UIViewContentModeScaleAspectFit;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(55, 8, 300, 30)];
        label.textColor = [UIColor whiteColor];
       // label.text = [items objectAtIndex:indexPath.row];
        //[cell.contentView addSubview:label];
        
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = [items objectAtIndex:indexPath.row];;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    return cell;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
