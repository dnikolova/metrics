//
//  SettingsViewController.m
//  MetricsApp
//
//  Created by Desi Nikolova on 3/27/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "SettingsViewController.h"
#import "DataManager.h"
#import "WebProperties.h"
#import "AppSettings.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    //
    _pickerView.alpha = 0.0;
    _doneBtn.alpha = 0.0;
    //
    CGRect rect = _pickerView.frame;
    rect.origin = CGPointMake(0, 305 + _pickerView.frame.size.height + 50);
    _pickerView.frame = rect;
    selectingSinceMonth = YES;
    _sinceMonthArray = [NSMutableArray arrayWithObjects:@"Jan",@"Feb",@"March",@"April",@"May",@"June",@"July",@"Aug",@"Sept",@"Oct",@"Nov",@"Dec", nil];
    _sinceYearArray = [NSMutableArray arrayWithObjects:@"2013",@"2012",@"2011",@"2010",@"2009",@"2008",@"2007",@"2006",@"2005",@"2004",@"2003",nil];
    
    _currentPeriod.selectedSegmentIndex = [AppSettings instance].analyticsChartPeriod;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"number of domains %d", [DataManager instance].webPropertiesWithData.count);
    
    return [DataManager instance].webPropertiesWithData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuCell";
	
	// Dequeue or create a cell of the appropriate type.
	UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        //
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(65, 8, 300, 30)];
        label.textColor = [UIColor whiteColor];
        label.text = @"";
        [cell.contentView addSubview:label];
    }
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    WebProperties *webProps = [[DataManager instance].webPropertiesWithData objectAtIndex:indexPath.row];
    //36/173/227
    UIColor *color = [UIColor colorWithRed:36.0/255.0 green:173.0/255.0 blue:227.0/255.0 alpha:1.0];
    if(!webProps.muted)
    {
        cell.textLabel.textColor = [UIColor whiteColor];
    }else{
        cell.textLabel.textColor = color;
    }
    cell.textLabel.text = webProps.assignedProfile.websiteUrl;
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    WebProperties *webProps = [[DataManager instance].webPropertiesWithData objectAtIndex:indexPath.row];
    UIColor *color = [UIColor colorWithRed:36.0/255.0 green:173.0/255.0 blue:227.0/255.0 alpha:1.0];
    if([cell.textLabel.textColor isEqual:color])
    {
        cell.textLabel.textColor = [UIColor whiteColor];
        webProps.muted = NO;
    }else{
        cell.textLabel.textColor = color;
        webProps.muted = YES;
    }
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void)reload
{
    [_tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_tableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSLog(@"the view will hide");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma Picker view delegate

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(selectingSinceMonth)
        selectedRow = [_sinceMonthArray objectAtIndex:row];
    else
        selectedRow = [_sinceYearArray objectAtIndex:row];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(selectingSinceMonth)
        return [_sinceMonthArray objectAtIndex:row];
    
    return [_sinceYearArray objectAtIndex:row];
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger) pickerView:(UIPickerView *)pickerView  numberOfRowsInComponent:(NSInteger)component
{
    if(selectingSinceMonth)
        return _sinceMonthArray.count;
    
    return _sinceYearArray.count;
}
 


#pragma mark -

-(IBAction)newPeriodSelection:(UISegmentedControl*)sender
{
    NSLog(@"new setting assigned for a period %d", sender.selectedSegmentIndex);
    [AppSettings instance].analyticsChartPeriod = sender.selectedSegmentIndex;
}


-(IBAction)sinceMonthBtnAction:(id)sender
{
    selectingSinceMonth = YES;
    [_pickerView reloadAllComponents];
    CGRect rect = _pickerView.frame;
    rect.origin = CGPointMake(0, 305);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:.44];
    _doneBtn.alpha = 1.0;
    _cancelbtn.alpha = 1.0;
    _pickerView.alpha = 1.0;
    _pickerView.frame = rect;
    [UIView commitAnimations];
}


-(IBAction)sinceYearBtnAction:(id)sender
{
    selectingSinceMonth = NO;
    [_pickerView reloadAllComponents];
    CGRect rect = _pickerView.frame;
    rect.origin = CGPointMake(0, 305);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:.44];
    _doneBtn.alpha = 1.0;
    _pickerView.alpha = 1.0;
    _cancelbtn.alpha = 1.0;
    _pickerView.frame = rect;
    [UIView commitAnimations];
}

-(IBAction)doneBtnAction:(id)sender
{
    CGRect rect = _pickerView.frame;
    rect.origin = CGPointMake(0, 305 + _pickerView.frame.size.height + 50);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:.44];
    _doneBtn.alpha = 0.0;
    _cancelbtn.alpha = 0.0;
    _pickerView.alpha = 0.0;
    _pickerView.frame = rect;
    [UIView commitAnimations];
    
    
    int row = [_pickerView selectedRowInComponent:0];
    
    if(selectingSinceMonth)
        selectedRow = [_sinceMonthArray objectAtIndex:row];
    else
        selectedRow = [_sinceYearArray objectAtIndex:row];
    
    if(selectingSinceMonth)
    {
        NSString *sinceMonth = [NSString stringWithFormat:@"Since %@",selectedRow ];
        [AppSettings instance].sinceMonth = sinceMonth;
        [_btnSinceMonth setTitle:selectedRow forState:UIControlStateNormal];
        //update segmented control and settings
        [_currentPeriod setTitle:sinceMonth forSegmentAtIndex:2];
    }else{
        NSString *sinceYear = [NSString stringWithFormat:@"Since %@",selectedRow ];
        [AppSettings instance].sinceYear = sinceYear;
        [_btnSinceYear setTitle:selectedRow forState:UIControlStateNormal];
        [_currentPeriod setTitle:sinceYear forSegmentAtIndex:3];
    }
    
    [AppSettings instance].periodOptions = [[NSMutableArray alloc] initWithObjects:@"Today",@"This Month",[AppSettings instance].sinceMonth, [AppSettings instance].sinceYear, nil];
}



-(IBAction)cancelBtnAction:(id)sender
{
    NSLog(@"cancel...");
    CGRect rect = _pickerView.frame;
    rect.origin = CGPointMake(0, 305 + _pickerView.frame.size.height + 50);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:.44];
    _cancelbtn.alpha = 0.0;
    _doneBtn.alpha = 0.0;
    _pickerView.alpha = 0.0;
    _pickerView.frame = rect;
    [UIView commitAnimations];
}


-(IBAction)goToDataViewBtnAction:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"GoToDataView" object:nil];
}


@end
