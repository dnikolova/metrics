//
//  ChartPoint.m
//  MetricsApp
//
//  Created by Desi Nikolova on 3/23/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "ChartPoint.h"

@implementation ChartPoint

@synthesize date, value;

-(id)initWithDate:(NSString*)_date value:(NSString*)_value
{
    self = [super init];
    self.date = _date;
    self.value = _value;
    
    return self;
}

@end
