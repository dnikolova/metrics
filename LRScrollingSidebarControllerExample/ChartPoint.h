//
//  ChartPoint.h
//  MetricsApp
//
//  Created by Desi Nikolova on 3/23/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChartPoint : NSObject
{
    
}

@property(nonatomic,retain)NSString *date;
@property(nonatomic, retain)NSString *value;

-(id)initWithDate:(NSString*)_date value:(NSString*)_value;

@end
