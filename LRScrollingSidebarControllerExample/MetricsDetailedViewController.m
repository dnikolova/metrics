//
//  MetricsDetailedViewController.m
//  MetricsApp
//
//  Created by Desi Nikolova on 3/26/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "MetricsDetailedViewController.h"
#import "CountryData.h"
#import "AppSettings.h"

@interface MetricsDetailedViewController ()

@end

@implementation MetricsDetailedViewController

@synthesize dataByCountry;
@synthesize assignedProfile;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [_tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reload) name:@"DataVisitsByCountryAvailable" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reload) name:@"DataVisitsPerPlatformTypeAvailable" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    loadingView.hidden = NO;
    loadingView.alpha = 1.0;
}

-(void)reload
{
    NSLog(@"reloading..");
    
    [_tableView reloadData];
    _activity.hidden = YES;
    
    _pieChartView.dataByCountry = self.dataByCountry;
    _pieChartView.assignedProperty = self.assignedProfile;
    [[AppSettings instance] loadColors];
    [_pieChartView  computeAngles];
    [_pieChartView setNeedsDisplay];
    
    NSString *selectedPeriod = [[AppSettings instance].periodOptions objectAtIndex:[AppSettings instance].analyticsChartPeriod];
    //
    selectedPeriodLabel.text = [NSString stringWithFormat:@"Visits %@", selectedPeriod];
    totalVisitsLabel.text = [NSString stringWithFormat:@"Total: %i", (int)assignedProfile.visits];
    
    [self showView];
}


-(void)showView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:.33];
    loadingView.alpha = 0.0;
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)backBtnAction:(id)sender
{
    [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(dataByCountry)
        return self.assignedProfile.countriesData.count;//
    
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuCell";
	
	// Dequeue or create a cell of the appropriate type.
	UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        //
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(65, 8, 300, 30)];
        label.textColor = [UIColor whiteColor];
        
        //[cell.contentView addSubview:label];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    if(dataByCountry)
    {
        CountryData *data = [self.assignedProfile.countriesData objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ - %i", data.countryName, data.visits];
        
        
    }else{
        if(indexPath.row == 0)
            cell.textLabel.text = [NSString stringWithFormat:@"Browsers - %i",assignedProfile.visitsFromBrowsers];
        else{
            cell.textLabel.text = [NSString stringWithFormat:@"Mobile devices - %i",assignedProfile.visitsFromMobileDevices];
        }
    }
    //
    BOOL colorIndicatorView = NO;
    if(indexPath.row < [AppSettings instance].pieChartColors.count)
    {
        
        UIColor *sectorColor = [[AppSettings instance].pieChartColors objectAtIndex:indexPath.row];
        for(UIView *view in cell.contentView.subviews)
        {
            if(view.tag == 333)
            {
                view.backgroundColor = sectorColor;
                colorIndicatorView = YES;
            }
        }
        
        if(!colorIndicatorView)
        {
            UIView *indicatorView = [[UIView alloc] initWithFrame:CGRectMake(260.0, 5.0, 50.0, 33)];
            [cell.contentView addSubview:indicatorView];
            indicatorView.tag = 333;
            indicatorView.backgroundColor = sectorColor;
        }
    }
    return cell;
    
}

-(IBAction)closeView:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseDetailedMetricsView" object:nil];
}

@end
