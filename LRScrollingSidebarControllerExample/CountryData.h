//
//  CountryData.h
//  Metrics
//
//  Created by Desi Nikolova on 3/31/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryData : NSObject

@property(nonatomic, retain)NSString *countryName;
@property(nonatomic, assign)int visits;

-(id)initWithCountry:(NSString*)country andVisits:(int)numVisits;

@end
