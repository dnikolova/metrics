//
//  HelpViewController.h
//  MetricsApp
//
//  Created by Desi Nikolova on 3/27/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HowTosViewController.h"

@interface HelpViewController : UIViewController
{
    HowTosViewController *_howTosViewController;
}

@end
