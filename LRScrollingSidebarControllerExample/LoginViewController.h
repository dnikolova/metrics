//
//  LoginViewController.h
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/4/14.
//  Copyright (c) 2014 Luis Recuenco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SigninViewController.h"

@class GTMOAuth2Authentication;

@interface LoginViewController : UIViewController
{
    SigninViewController *_signinViewController;
    IBOutlet UIActivityIndicatorView *_activityView;
    
    int mNetworkActivityCounter;
    GTMOAuth2Authentication *mAuth;
    IBOutlet UIButton *signInBtn;
    BOOL signedOut;
}

-(void)registerInKeychain;
-(void)updateUI;
- (void)displayAlertWithMessage:(NSString *)message;
- (void)saveClientIDValues;
- (void)loadClientIDValues;
- (void)incrementNetworkActivity:(NSNotification *)notify;

- (void)decrementNetworkActivity:(NSNotification *)notify;
- (void)signInNetworkLostOrFound:(NSNotification *)notify;
- (void)signOut;
- (void)showLoginBtn;

@property (nonatomic, retain) GTMOAuth2Authentication *auth;

@end
