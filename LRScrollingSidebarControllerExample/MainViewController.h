// MainViewController.h
//

#import "LRScrollingSidebarController.h"
#import "HelpViewController.h"
#import "SettingsViewController.h"
#import "EducationViewController.h"

enum{
  SECTION_ANALYTICS = 111,
  SECTION_SETTINGS,
  SECTION_HELP,
  SECTION_MORE_METRICS,
  SECTION_EDUCATION
}appSection;

@class WebPropertyViewController;
@class MoreMetricsViewController;

@interface MainViewController : UIViewController <LRScrollingSidebarController>
{
    int currentSection;
    WebPropertyViewController *webPropertyViewController;
    IBOutlet UIButton *menuButton;
    HelpViewController *helpViewController;
    SettingsViewController *settingsViewController;
    EducationViewController *educationViewController;
    MoreMetricsViewController *moreMetricsViewController;
}

-(void)displaySection:(int)section;

@end
