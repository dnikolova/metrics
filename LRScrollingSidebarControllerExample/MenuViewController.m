// LeftViewController.m
//
// Copyright (c) 2014 Luis Recuenco
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "MenuViewController.h"
#import "MainViewController.h"
#import "UIViewController+LRScrollingSidebarController.h"

@implementation MenuViewController


#define DEBUG_MODE 0 //0


-(void)viewDidLoad
{
    [super viewDidLoad];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    menuItems = [NSArray arrayWithObjects:@"My websites' data", /*@"Adsense",@"SEO",*/ @"Settings",@"Help",@"More metrics", @"Education",nil];
    [_tableView reloadData];
    
    if(DEBUG_MODE)
        _errorsLogView.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToSettings) name:@"GoToSettings" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToDataView) name:@"GoToDataView" object:nil];
}

-(void)goToDataView
{
    [self.scrollingSidebarController sectionInMainView:0+111];
    [self showMainPanel];
}

-(void)goToSettings
{
    [self.scrollingSidebarController sectionInMainView:1+111];
    [self showMainPanel];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuItems.count;//
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MenuCell";
	
	// Dequeue or create a cell of the appropriate type.
	UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dataIcon.png"]];
        icon.tag = 88;
        [cell.contentView addSubview:icon];
        
        if(indexPath.row == 1)
            icon.image = [UIImage imageNamed:@"settingsIcon.png"];
        if(indexPath.row == 2)
           icon.image = [UIImage imageNamed:@"helpIcon.png"];
        if(indexPath.row == 3)
            icon.image = [UIImage imageNamed:@"add_icon.png"];
        if(indexPath.row == 4)
           icon.image = [UIImage imageNamed:@"educationIcon.png"];
        
        CGRect frame = icon.frame;
        frame.origin = CGPointMake(12, 10);
        frame.size = CGSizeMake(CGImageGetWidth(icon.image.CGImage) / 2, CGImageGetHeight(icon.image.CGImage) / 2);
        icon.frame = frame;
        //icon.contentMode = UIViewContentModeScaleAspectFit;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(55, 8, 300, 30)];
        label.textColor = [UIColor whiteColor];
        label.text = [menuItems objectAtIndex:indexPath.row];
        [cell.contentView addSubview:label];
        
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = @"";
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    return cell;
    
}

-(void)canShowMenu
{
    //configure;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.scrollingSidebarController sectionInMainView:indexPath.row+111];
    [self showMainPanel];
}



- (void)showMainPanel
{
    [self.scrollingSidebarController showMainViewControllerAnimated:YES];
}

-(IBAction)signOut:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Signing out" message:@"Do you really want to sign out? This action will clear your current session." delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = 111;
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 111)
    {
        if(buttonIndex == 1)
            [self.scrollingSidebarController signOutBtnAction];
    }
    
}

- (IBAction)replaceMainPanelButtonTapped:(id)sender
{
    MainViewController *mainViewController = [[MainViewController alloc] init];
    mainViewController.view.backgroundColor = LRGetRandomColor();
    [self.scrollingSidebarController showMainViewController:mainViewController animated:YES];
}

#pragma mark - LRScrollingSidebarController

- (void)controllerIsVisible:(BOOL)controllerIsVisible
{
    NSLog(@"Left view controller is visible: %d", controllerIsVisible);
}

-(void)logError:(NSString*)error
{
    _errorsLogView.text = error;
}

@end
