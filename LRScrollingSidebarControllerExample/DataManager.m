//
//  DataManager.m
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/13/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import "DataManager.h"
#import "GTMOAuth2Authentication.h"
#import "AppSettings.h"
#import "WebProperties.h"
#import "ChartPoint.h"
#import "CountryData.h"
#import "Metrics&Dimensions.h"

@implementation DataManager

@synthesize auth = mAuth;
@synthesize webPropertiesWithData;
@synthesize _currentProperty;
@synthesize inAppPurchasesMoreMetrics, inAppPurchasesSessionExtensions;

static DataManager *m_instance = nil;


-(id)init
{
    self = [super init];
    webPropertiesWithData = [[NSMutableArray alloc] init];
    dataPrcessingStep = PERIOD_IS_TODAY;
    [self setDates];
    [self loadPurchasesAndPurchasedItems];
    
    return self;
}

+(DataManager*)instance
{
    if(!m_instance)
        m_instance = [[DataManager alloc] init];
    
    return m_instance;
}

/* In-app purchased - initial setup */

-(void)loadPurchasesAndPurchasedItems
{
    // Adsense
    NSMutableDictionary *adsense = [[NSMutableDictionary alloc] init];
    [adsense setObject:@"AdSense" forKey:@"title"];
    NSArray *adsense_metrics = [NSArray arrayWithObjects:kGAAdsenseRevenue,kGAAdsenseAdsViewed, kGAAdsenseAdsClicks, kGAAdsenseAdUnitsViewed, nil];
    NSArray *adsense_metrics_names = [NSArray arrayWithObjects:@"Adsense Revenue",@"Adsense Ads Viewed", @"Adsense Ads Clicks", @"Adsense Ad Units Viewed", nil];
    [adsense setObject:adsense_metrics forKey:@"metrics"];
    [adsense setObject:adsense_metrics_names forKey:@"metrics_names"];
    
    //Audience
    
    NSMutableDictionary *audience = [[NSMutableDictionary alloc] init];
    [audience setObject:@"Audience" forKey:@"title"];
    NSArray *audience_metrics = [NSArray arrayWithObjects:kGAVisitorAgeBracket,kGAVisitorGender, nil];
    NSArray *audience_metrics_names = [NSArray arrayWithObjects:@"Visitor Age Bracket",@"Visitor Gender", nil];
    [audience setObject:audience_metrics forKey:@"metrics"];
    [audience setObject:audience_metrics_names forKey:@"metrics_names"];
    
    //Geo Network

    NSMutableDictionary *geo = [[NSMutableDictionary alloc] init];
    [geo setObject:@"Geo Network" forKey:@"title"];
    NSArray *geo_metrics = [NSArray arrayWithObjects:kGACity,kGARegion,kGALatitude,kGALongitude,kGANetworkDomain,kGANetworkLocation, nil];
    NSArray *geo_metrics_names = [NSArray arrayWithObjects:@"City",@"Region",@"Latitude",@"Longitude",@"Network Domain",@"Network Location", nil];
    [geo setObject:geo_metrics forKey:@"metrics"];
    [geo setObject:geo_metrics_names forKey:@"metrics_names"];
    
    //Mobile device branding
    NSMutableDictionary *mobileBranding = [[NSMutableDictionary alloc] init];
    [mobileBranding setObject:@"Mobile device branding" forKey:@"title"];
    NSArray *mobileBranding_metrics = [NSArray arrayWithObjects:kGAMobileDeviceBranding, nil];
    NSArray *mobileBranding_metrics_names = [NSArray arrayWithObjects:@"Device brand", nil];
    [mobileBranding setObject:mobileBranding_metrics forKey:@"metrics"];
    [mobileBranding setObject:mobileBranding_metrics_names forKey:@"metrics_names"];
    
    //System
    NSMutableDictionary *system = [[NSMutableDictionary alloc] init];
    [system setObject:@"System" forKey:@"title"];
    NSArray *system_metrics = [NSArray arrayWithObjects:kGAFlashVersion, kGAJavaEnabled, kGALanguage, nil];
    NSArray *system_metrics_names = [NSArray arrayWithObjects:@"Flash Version",@"Java Enabled",@"Language", nil];
    [system setObject:system_metrics forKey:@"metrics"];
    [system setObject:system_metrics_names forKey:@"metrics_names"];
  
    //Exceptions
    NSMutableDictionary *exceptions = [[NSMutableDictionary alloc] init];
    [exceptions setObject:@"Exceptions" forKey:@"title"];
    NSArray *exceptions_metrics = [NSArray arrayWithObjects:kGAExceptions, kGAFatalExceptions, nil];
    NSArray *exceptions_metrics_names = [NSArray arrayWithObjects:@"Exceptions", @"Fatal Exceptions", nil];
    [exceptions setObject:exceptions_metrics forKey:@"metrics"];
    [exceptions setObject:exceptions_metrics_names forKey:@"metrics_names"];
   
    inAppPurchasesMoreMetrics = [[NSMutableArray alloc] initWithObjects:adsense,audience,geo,mobileBranding,system,exceptions, nil];
}


/* Google Queries */

- (void)analyticsQuery
{
    service = [[GTLServiceAnalytics alloc] init];
    service.authorizer = self.auth;
    //NSLog(@"auth %@ %@", self.auth.userID, self.auth.userAgent);
    // GTLQueryAnalytics *query = [GTLQueryAnalytics queryForDataGaGetWithIds:@"ga:79891549" startDate:@"2006-01-01" endDate:@"today" metrics:@"ga:totalEvents"];
    GTLQueryAnalytics *query = [GTLQueryAnalytics queryForManagementAccountsList];
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    GTLServiceTicket *ticket = [service executeQuery:query
                                   completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
    if (error == nil) {
                                           
        accounts = object;
        //NSLog(@"query is good - %@ %@; account items: %@; account items perpage - %@", [object description], object, accounts.items, accounts.itemsPerPage);
        [self retrieveAccountData];
                                           
         [appDelegate logError:@"services query good to go"];
    }else{
        NSLog(@"error %@", error.description);
                                           
         [appDelegate logError:error.description];
        }
    }];
    
}

-(void)retrieveAccountData
{
    //here takes the domains and the data associated with each:
    //see specs - visits, period, also metrics for up or down ..
    for(id item in accounts.items)
    {
        GTLAnalyticsAccount *account = item;
        
        NSLog(@"account item %@; name %@", account.identifier, account.name);
        
        GTLQueryAnalytics *query = [GTLQueryAnalytics queryForManagementProfilesListWithAccountId:@"~all" webPropertyId:@"~all"];
        //
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        GTLServiceTicket *ticket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        if (error == nil) {
                                           
                NSLog(@"data web properties - %@", object);
                myWebProperties = object;
            
                [appDelegate logError:@"services query good to go"];
            [self getWebPropertiesData];
        }else
            {
                NSLog(@" data web properties - error %@", error.description);
                                           
                [appDelegate logError:error.description];
            }
        }];
        
        break;//displaying one!
    }

}

-(void)getWebPropertiesData
{
    for(id webProperty in myWebProperties)
    {
        //NSLog(@"prop %@", webProperty);
        GTLAnalyticsProfile *prop = webProperty;
        
        WebProperties *webP = [[WebProperties alloc] init];
        webP.assignedProfile = prop;
        
        [webPropertiesWithData addObject:webP];
    }
    if(webPropertiesWithData.count)
        _currentProperty = [webPropertiesWithData objectAtIndex:0];
    else
        return;
    
    [self resetCounters];
    [self loadVisits];
}


-(void)refreshData
{
    //reset everything
    [self resetCounters];
    [self loadVisits];
}


-(void)load
{
   // if(loading)
     //cancel;
    [self loadVisits];
}


-(void)loadVisits
{
    WebProperties *webP = _currentProperty;
    GTLAnalyticsProfile *prop = webP.assignedProfile;
            
    //just today
    NSString *endDate = @"today";
    NSString *beginDate = [self getBeginDate];
    
    GTLQueryAnalytics *query = [GTLQueryAnalytics queryForDataGaGetWithIds:[NSString stringWithFormat:@"ga:%@",prop.identifier]startDate:beginDate endDate:endDate metrics:kGAVisits];
    
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        GTLServiceTicket *ticket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
            if (error == nil)
            {
                GTLAnalyticsGaData *data = object;
                NSLog(@"data%@", object);
                GTLAnalyticsGaDataTotalsForAllResults *totalsForAllResults = data.totalsForAllResults;
                NSMutableDictionary *dict = totalsForAllResults.JSON;
                
                NSNumber *visits = [dict objectForKey:kGAVisits];
                
                webP.visits = visits.floatValue;
                
                //NSLog(@"web visits for %@ are %f %@", webP.assignedProfile.websiteUrl, visits.floatValue, prop.identifier);
                [appDelegate logError:@"services query good to go"];
                
                [self loadVisitors];
            }else
            {
                NSLog(@" data web properties - error %@", error.description);
                
                [appDelegate logError:error.description];
            }
        }];

}


-(void)loadVisitors
{
    WebProperties *webP = _currentProperty;
        GTLAnalyticsProfile *prop = webP.assignedProfile;
        
        //just today
        NSString *endDate = @"today";
        NSString *beginDate = [self getBeginDate];
    
        GTLQueryAnalytics *query = [GTLQueryAnalytics queryForDataGaGetWithIds:[NSString stringWithFormat:@"ga:%@",prop.identifier]startDate:beginDate endDate:endDate metrics:kGAVisitors];
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        GTLServiceTicket *ticket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
            if (error == nil)
            {
                GTLAnalyticsGaData *data = object;
                GTLAnalyticsGaDataTotalsForAllResults *totalsForAllResults = data.totalsForAllResults;
                NSMutableDictionary *dict = totalsForAllResults.JSON;
                NSLog(@"result %@", dict.description);
                NSNumber *visitors = [dict objectForKey:kGAVisitors];
                webP.visitors = visitors.floatValue;
                //
                NSLog(@"web visitors for %@ are %f", webP.assignedProfile.websiteUrl, visitors.floatValue);
                
                [self loadPageViews];
                [appDelegate logError:@"services query good to go"];
            }else
            {
                NSLog(@" data web properties - error %@", error.description);
                
                [appDelegate logError:error.description];
            }
        }];
    
}

-(void)loadPageViews
{
    WebProperties *webP = [webPropertiesWithData objectAtIndex:todaysPages];
    GTLAnalyticsProfile *prop = webP.assignedProfile;
        
        //just today
        NSString *endDate = @"today";
        NSString *beginDate = [self getBeginDate];
       //
        GTLQueryAnalytics *query = [GTLQueryAnalytics queryForDataGaGetWithIds:[NSString stringWithFormat:@"ga:%@",prop.identifier]startDate:beginDate endDate:endDate metrics:kGAPageviews];
        
        AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        GTLServiceTicket *ticket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
            if (error == nil)
            {
                GTLAnalyticsGaData *data = object;
                GTLAnalyticsGaDataTotalsForAllResults *totalsForAllResults = data.totalsForAllResults;
                NSMutableDictionary *dict = totalsForAllResults.JSON;
                NSLog(@"result %@", dict.description);
                NSNumber *pageviews = [dict objectForKey:kGAPageviews];
                
                webP.pageviews = pageviews.floatValue;
                
                NSLog(@"pageviews for %@ are %f", webP.assignedProfile.websiteUrl, pageviews.floatValue);
                
                NSLog(@"pageview %i %i", todaysPages, webPropertiesWithData.count);
                [self loadAnalyticsChart];
                //
                [appDelegate logError:@"services query good to go"];
            }else
            {
                NSLog(@" data web properties - error %@", error.description);
                [appDelegate logError:error.description];
            }
        }];
    
}



-(void)loadAnalyticsChart
{
    WebProperties *webP = _currentProperty;
    GTLAnalyticsProfile *prop = webP.assignedProfile;
    
    //This month || This Year - 2 modes for now
    NSString *endDate = @"today";
    //here can switch between a year and a month looks like
    NSString *beginDate = [self getBeginDate];
    
    //may have to adjust some more!
    
    NSLog(@"Querying the chart data %@ - %@", endDate, beginDate);
    /*
     ['https://www.google.com/analytics/feeds/data',
     '?start-date=2010-06-01',
     '&end-date=2010-06-10',
     '&dimensions=ga:day,ga:visitorType',
     '&metrics=ga:visits',
     '&sort=ga:day',
     '&max-results=20',
     '&ids=', document.getElementById('tableId').value].join('');
     */
    
    NSLog(@"qurying for a chart ! - table data !");
    
    NSString *metricsAndDimens = [NSString stringWithFormat:@"%@", kGAVisits];
   //
    GTLQueryAnalytics *query = [GTLQueryAnalytics queryForDataGaGetWithIds:[NSString stringWithFormat:@"ga:%@",prop.identifier] startDate:beginDate endDate:endDate metrics:metricsAndDimens];
    
    /* Dimensions rules */
    /*
    if since year ( dimenstions = @"ga:year", since month -
                   @"ga:month" /this year/ and this month -> @"ga:day" /days from the last month and the same to use for the 'today' unless there is a hourly breakdown/
     */
    if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_TODAY)
    {
        query.dimensions = @"ga:hour";
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_THIS_MONTH)
    {
        query.dimensions = @"ga:day";
        beginDate = [self thisMonth];
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_MONTH)
    {
        query.dimensions = @"ga:month";
    }
    else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_YEAR)
    {
        query.dimensions = @"ga:year";
    }

    query.maxResults = 20;
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    GTLServiceTicket *ticket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error)
    {
        if (error == nil)
        {
            GTLAnalyticsGaData *data = object;
            webP.chartData = [[NSMutableArray alloc] init];
            NSLog(@"ticket %@", ticket);
            
            for( id row in data.rows)
            {
                NSLog(@"row: day:%@  visits:%@", row[0], row[1]);
                ChartPoint *chPoint = [[ChartPoint alloc] initWithDate:row[0] value:row[1]];
                [webP.chartData addObject:chPoint];
            }
            /*
            
            for( id col in data.columnHeaders)
            {
                NSLog(@"column: %@", col);
            }
            */
            //
            NSLog(@"Chart result! %d", webP.chartData.count);
            [appDelegate logError:@"services query good to go"];
            ///////////////////////////////////////////////////
            
            
        }else
        {
            NSLog(@"Chart query error %@", error.description);
            [appDelegate logError:error.description];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadDone" object:nil];
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AnalyticsChartDataAvailable" object:nil];
}


-(void)loadVisitsByCountryForProfile:(WebProperties*)webP
{
    GTLAnalyticsProfile *prop = webP.assignedProfile;
    
    //just today
    NSString *endDate = @"today";
    NSString *beginDate = [self getBeginDate];
    
    
    GTLQueryAnalytics *query = [GTLQueryAnalytics queryForDataGaGetWithIds:[NSString stringWithFormat:@"ga:%@",prop.identifier]startDate:beginDate endDate:endDate metrics:kGAVisits];
    
    query.dimensions = kGADCountry;
    query.sort= @"-ga:visits";
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    GTLServiceTicket *ticket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        if (error == nil)
        {
            GTLAnalyticsGaData *data = object;
            //NSLog(@"data%@", object);
            GTLAnalyticsGaDataTotalsForAllResults *totalsForAllResults = data.totalsForAllResults;
            NSMutableDictionary *dict = totalsForAllResults.JSON;
            
            webP.countriesData = [[NSMutableArray alloc] init];
            
            for( id row in data.rows)
            {
                NSLog(@"row: country:%@  visits:%@", row[0], row[1]);
                NSString *visits = row[1];
                CountryData *countryData = [[CountryData alloc] initWithCountry:row[0] andVisits:visits.intValue];
                [webP.countriesData addObject:countryData];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataVisitsByCountryAvailable" object:nil];
            
        }else
        {
            NSLog(@" data web properties - error %@", error.description);
            
            [appDelegate logError:error.description];
        }
    }];
    

}


-(void)loadVisitsWithPlatformInfoForProfile:(WebProperties*)webP
{
    GTLAnalyticsProfile *prop = webP.assignedProfile;
    
    //just today
    NSString *endDate = @"today";
    NSString *beginDate = [self getBeginDate];
    GTLQueryAnalytics *query = [GTLQueryAnalytics queryForDataGaGetWithIds:[NSString stringWithFormat:@"ga:%@",prop.identifier]startDate:beginDate endDate:endDate metrics:kGAVisits];
    
    query.dimensions = kGADeviceIsMobile;
    query.sort= @"-ga:visits";
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    GTLServiceTicket *ticket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id object, NSError *error) {
        if (error == nil)
        {
            GTLAnalyticsGaData *data = object;
            //NSLog(@"data%@", object);
            GTLAnalyticsGaDataTotalsForAllResults *totalsForAllResults = data.totalsForAllResults;
            NSMutableDictionary *dict = totalsForAllResults.JSON;
            
            for( id row in data.rows)
            {
                NSLog(@"row: is mobile :%@  visits:%@", row[0], row[1]);
                NSString *isMobile = row[0];
                if([isMobile isEqualToString:@"No"])
                {
                    NSString *visits = row[1];
                    webP.visitsFromBrowsers = visits.intValue;
                }else{
                    NSString *visits = row[1];
                    webP.visitsFromMobileDevices = visits.intValue;
                }
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataVisitsPerPlatformTypeAvailable" object:nil];
            
        }else
        {
            NSLog(@" data web properties - error %@", error.description);
            
            [appDelegate logError:error.description];
        }
    }];
    
    
}



//Use the NSCalender:


-(NSString*)getBeginDate
{
    NSString *date = @"2011-01-01";
    if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_TODAY)
    {
        date = @"today";
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_THIS_MONTH)
    {
        date = [self thisMonth];
    }else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_MONTH)
    {
        NSString *thisMonth = [self thisMonth];
        //strip off the year!
        date = [NSString stringWithFormat:@"%@-01-%@",[thisMonth substringToIndex:4], [[AppSettings instance] monthToNumber]];
    }
    else if([AppSettings instance].analyticsChartPeriod == PERIOD_IS_SINCE_YEAR)
    {
        date = [NSString stringWithFormat:@"%@-01-01",[[AppSettings instance] yearToNumber]];
    }

    NSLog(@"Current date %@", date);
    //
    return date;
}


-(NSString*)today
{
    return [formatter stringFromDate:today];//this goes as @today but in case.
}

-(NSString*)yesterday
{
    return [formatter stringFromDate:yesterday];
}

-(NSString*)thisMonth
{
    return [formatter stringFromDate:thisMonth];
}

-(NSString*)previousMonth
{
    return [formatter stringFromDate:lastMonth];
}

-(NSString*)dayBeforeDate:(NSDate*)date
{
    return @"";
}

-(NSString*)monthBeforeDate:(NSDate*)date
{
    return @"";
}

-(void)setDates
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[[NSDate alloc] init]];
    
    [components setHour:-[components hour]];
    [components setMinute:-[components minute]];
    [components setSecond:-[components second]];
    today = [cal dateByAddingComponents:components toDate:[[NSDate alloc] init] options:0]; //This variable should now be pointing at a date object that is the start of today (midnight);
    
    [components setHour:-24];
    [components setMinute:0];
    [components setSecond:0];
    yesterday = [cal dateByAddingComponents:components toDate: today options:0];
    
    components = [cal components:NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[[NSDate alloc] init]];
    
    [components setDay:([components day] - ([components weekday] - 1))];
    thisWeek  = [cal dateFromComponents:components];
    
    [components setDay:([components day] - 7)];
    lastWeek  = [cal dateFromComponents:components];
    
    [components setDay:([components day] - ([components day] -1))];
    thisMonth = [cal dateFromComponents:components];
    
    [components setMonth:([components month] - 1)];
    lastMonth = [cal dateFromComponents:components];
    
    //how to use
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSLog(@"today=%@",[formatter stringFromDate:today]);
    NSLog(@"yesterday=%@",[formatter stringFromDate:yesterday]);
    //NSLog(@"thisWeek=%@",thisWeek);
    //NSLog(@"lastWeek=%@",lastWeek);
    NSLog(@"thisMonth=%@",[formatter stringFromDate:thisMonth]);
    NSLog(@"lastMonth=%@",[formatter stringFromDate:lastMonth]);
}

-(void)resetCounters
{
    todaysVisits = 0;
    todaysVisitors = 0;
    todaysPages = 0;
    ystdVisits = 0;
    ystdVisitors = 0;
    ystdPages  = 0;
}

@end
