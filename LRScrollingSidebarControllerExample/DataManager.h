//
//  DataManager.h
//  AnalyticsApp
//
//  Created by Desi Nikolova on 3/13/14.
//  Copyright (c) 2014 Desi Nikolova. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppDelegate.h"
#import "GTLAnalytics.h"
#import "GTLServiceAnalytics.h"
/*
enum
{
    PERIOD_TODAY = 999,
    PERIOD_YESTERDAY,
    PERIOD_THIS_MONTH,
    PERIOD_LAST_MONTH
}periodLen;
*/

@class WebProperties;

@interface DataManager : NSObject
{
    GTMOAuth2Authentication *mAuth;
    GTLAnalyticsAccounts *accounts;
    GTLServiceAnalytics *service;
    GTLAnalyticsProfiles *myWebProperties;
    int dataPrcessingStep;
    int visitsLoaded;
    NSDate *today;
    NSDate *yesterday;
    NSDate *thisWeek;
    NSDate *lastWeek;
    NSDate *thisMonth;
    NSDate *lastMonth;
    NSDateFormatter *formatter;
    BOOL allLoaded;
    int todaysVisits, todaysVisitors, todaysPages, ystdVisits, ystdVisitors, ystdPages;
    BOOL loading;
    
}

@property (nonatomic, retain)WebProperties *_currentProperty;
@property (nonatomic, retain) GTMOAuth2Authentication *auth;
@property (nonatomic, retain) NSMutableArray *webPropertiesWithData;
//In-app purchses:
@property (nonatomic, retain) NSMutableArray *inAppPurchasesMoreMetrics;
@property (nonatomic, retain) NSMutableArray *inAppPurchasesSessionExtensions;//managing rights, i.e. the management API.

+ (DataManager*)instance;
- (void)analyticsQuery;
- (NSString*)yesterday;
- (void)loadPageViews:(int)period;
- (void)loadVisitsByCountryForProfile:(WebProperties*)webP;
- (void)loadVisitsWithPlatformInfoForProfile:(WebProperties*)webP;
- (void)refreshData;
- (void)load;

@end
